<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class AuditController extends Controller
{
    /**
     * Get Audit log entries from Database
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        // get token from session
        if (!$this->container->get('session')->has('apiToken')) {
            return $this->redirectToRoute('login');
        }

        // get entity manager
        $entityManager = $this->getDoctrine()->getManager();

        // get audit log entries
        $entries = $entityManager
            ->getRepository('AppBundle:AuditLogEntry')
            ->findBy(array(), array('id' => 'DESC'));

        return $this->render(
            'log/audit.html.twig',
            array('entries' => $entries)
        );
    }

}