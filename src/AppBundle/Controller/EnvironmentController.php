<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class EnvironmentController extends Controller
{
    /**
     * Get Environments information from API
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        // get token from session
        $apiToken = $this->container->get('session')->get('apiToken');
        // get environments list
        $response = $this->get('environment_manager')->getEnvironments($apiToken);

        // if token expired or call failed redirect to login page
        if (!$response || !empty($response->error)) {
            return $this->redirectToRoute('login');
        } else {
            // render page if everything is okay
            return $this->render(
                'environment/environment.html.twig',
                array(
                    'env_results'   => $response,
                    'columns_names' => array_keys(get_object_vars($response[0])),
                )
            );
        }
    }

    /**
     * Edit an environment
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function editAction(Request $request)
    {
        // get token from session
        $apiToken = $this->container->get('session')->get('apiToken');

        // get environment object from request
        $envObject = (object)$request->request->all();

        // get the original object
        $originalObj = $this->get('environment_manager')
            ->getEnvironmentById($apiToken, $envObject->id);

        // update the environment
        $response = $this->get('environment_manager')
            ->updateEnvironment($envObject, $apiToken);

        // if token expired or call failed redirect to login page
        if (!empty($response->error)
            && !empty($response->code)
            && $response->code == 'access_token'
        ) {
            return $this->redirectToRoute('login');
        } else {

            if (empty($response->error)) {
                // log changes done to object
                $this->get('audit_log')->logChanges($originalObj, $envObject);
            }

            // success or failure message
            $message = empty($response->error) ? 'Environment updated' : json_encode($response->error);

            return $this->render(
                'environment/environment_edit.html.twig',
                array(
                    'env_object'   => $envObject,
                    'message'      => $message,
                )
            );
        }
    }

    /**
     * Show single environment details
     * @param Request $request - request should include the environment id.
     * @return Response
     */
    public function showAction(Request $request)
    {
        // get token from session
        $apiToken = $this->container->get('session')->get('apiToken');
        // get environment id from request
        $environmentId = $request->get('environment_id');
        // get object from API
        $envObject = $this->get('environment_manager')->getEnvironmentById($apiToken, $environmentId);

        // if token expired or call failed redirect to login page
        if (!$envObject) {
            return $this->redirectToRoute('login');
        } else {
            return $this->render(
                'environment/environment_edit.html.twig',
                array(
                    'env_object' => $envObject,
                    'message' => '',
                )
            );
        }
    }

    /**
     * Revert a specific field on an environment object
     * @param Request $request
     * @return Response
     */
    public function revertAction(Request $request)
    {
        // get token from session
        $apiToken = $this->container->get('session')->get('apiToken');

        // get fields from request
        $auditLogId = $request->get('audit_id');

        $em = $this->getDoctrine()->getManager();
        $auditEntry = $em->getRepository('AppBundle:AuditLogEntry')->find($auditLogId);

        // revert changes
        $response = $this->get('environment_manager')
            ->updateSingleField(
                $apiToken,
                $auditEntry->getEnvironmentId(),
                $auditEntry->getFieldName(),
                $auditEntry->getChangedFrom()
            );

        if (!$response) {
            return $this->redirectToRoute('login');
        } else {
            // change rollback status
            $auditEntry->setRollBacked(true);
            $em->flush();

            return $this->redirectToRoute(
                'environment_show',
                array('environment_id' => $auditEntry->getEnvironmentId())
            );
        }
    }
}