<?php

namespace AppBundle\Controller;

use Lsw\ApiCallerBundle\Call\HttpGetJson;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;

class WorkerController extends Controller
{
    /**
     * Get Workers information from API
     * @param Request $request
     * @return Response
     */
    public function indexAction(Request $request)
    {
        // get token from session
        $apiToken = $this->container->get('session')->get('apiToken');

        $response = $this->get('worker_manager')->getWorkers($apiToken);

        // if token expired or call failed redirect to login page
        if (!$response || !empty($response->error)) {
            return $this->redirectToRoute('login');
        } else {
            // render page if everything is okay
            return $this->render(
                'workers/worker.html.twig',
                array(
                    'env_results'   => $response,
                    'columns_names' => array_keys(get_object_vars($response[0])),
                )
            );
        }

    }
}