<?php

namespace AppBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Lsw\ApiCallerBundle\Call\HttpGetJson;

class LoginController extends Controller
{
    /**
     * Render the login template
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function loginAction(Request $request)
    {
        $session = $this->container->get('session');
        $session->remove('apiToken');

        return $this->render(
            'validation/login.html.twig',
            array(
                // last username entered by the user
                'last_username' => '',
                'error' => '',
            )
        );
    }

    /**
     * Verify User against API
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\RedirectResponse|Response
     */
    public function verifyAction(Request $request)
    {
        $username = $request->get('_username');
        $password = $request->get('_password');
        $relativeUrl = 'api/user/login';

        // api login call
        $response = $this->get('elyotech_connector')->login($relativeUrl, $username, $password);

        // if request to API failed reload page with errors
        if (!$response || !empty($response->error)) {
            $error = 'Invalid username or password';

            return $this->render(
                'validation/login.html.twig',
                array(
                    // last username entered by the user
                    'last_username' => $username,
                    'error' => $error,
                )
            );
        } else {
            $session = $this->container->get('session');
            $session->set('apiToken', $response->token);

            return $this->redirectToRoute('homepage');
        }
    }
}