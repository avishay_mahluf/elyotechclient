<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="auditlog")
 */
class AuditLogEntry
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $fieldName;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $changedFrom;

    /**
     * @ORM\Column(type="string", length=100)
     */
    protected $changedTo;

    /**
     * @ORM\Column(type="integer")
     */
    protected $environmentId;

    /**
     * @ORM\Column(type="boolean")
     */
    protected $rollBacked;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $created;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fieldName
     *
     * @param string $fieldName
     *
     * @return AuditLogEntry
     */
    public function setFieldName($fieldName)
    {
        $this->fieldName = $fieldName;

        return $this;
    }

    /**
     * Get fieldName
     *
     * @return string
     */
    public function getFieldName()
    {
        return $this->fieldName;
    }

    /**
     * Set changedFrom
     *
     * @param string $changedFrom
     *
     * @return AuditLogEntry
     */
    public function setChangedFrom($changedFrom)
    {
        $this->changedFrom = $changedFrom;

        return $this;
    }

    /**
     * Get changedFrom
     *
     * @return string
     */
    public function getChangedFrom()
    {
        return $this->changedFrom;
    }

    /**
     * Set changedTo
     *
     * @param string $changedTo
     *
     * @return AuditLogEntry
     */
    public function setChangedTo($changedTo)
    {
        $this->changedTo = $changedTo;

        return $this;
    }

    /**
     * Get changedTo
     *
     * @return string
     */
    public function getChangedTo()
    {
        return $this->changedTo;
    }

    /**
     * Set environmentId
     *
     * @param integer $environmentId
     *
     * @return AuditLogEntry
     */
    public function setEnvironmentId($environmentId)
    {
        $this->environmentId = $environmentId;

        return $this;
    }

    /**
     * Get environmentId
     *
     * @return integer
     */
    public function getEnvironmentId()
    {
        return $this->environmentId;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     *
     * @return AuditLogEntry
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set rollBacked
     *
     * @param boolean $rollBacked
     *
     * @return AuditLogEntry
     */
    public function setRollBacked($rollBacked)
    {
        $this->rollBacked = $rollBacked;

        return $this;
    }

    /**
     * Get rollBacked
     *
     * @return boolean
     */
    public function getRollBacked()
    {
        return $this->rollBacked;
    }
}
