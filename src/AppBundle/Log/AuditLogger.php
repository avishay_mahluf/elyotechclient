<?php

namespace AppBundle\Log;

use AppBundle\Entity\AuditLogEntry;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Config\Definition\Exception\Exception;

class AuditLogger
{
    /**
     * @var EntityManager
     */
    private $entityManager;

    /**
     * @param EntityManager $entityManager dependency injected
     */
    public function __construct(EntityManager $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * Finds differences of 2 associative arrays and log them to DB
     * @param $objBefore - the object before the change
     * @param $objAfter - the object after the change
     */
    public function logChanges($objBefore, $objAfter)
    {
        // extract all keys of array
        $props = array_keys(get_object_vars($objBefore));

        // loop on every property and check if there was a change
        foreach ($props as $prop) {
            // check for changes
            if ($objBefore->$prop != $objAfter->$prop) {
                // log a new entry
                $auditLogEntry = new AuditLogEntry();

                $auditLogEntry->setFieldName($prop);
                $auditLogEntry->setChangedFrom($objBefore->$prop);
                $auditLogEntry->setChangedTo($objAfter->$prop);
                // saving id assuming ID field will never be removed from object
                $auditLogEntry->setEnvironmentId($objBefore->id);
                $auditLogEntry->setRollBacked(false);
                $auditLogEntry->setCreated(new \DateTime());

                try {
                    // save to DB
                    $this->entityManager->persist($auditLogEntry);
                    $this->entityManager->flush();
                } catch (Exception $e) {
                    continue;
                }
            }
        }
    }
}