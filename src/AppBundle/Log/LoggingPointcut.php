<?php

namespace AppBundle\Log;

use JMS\AopBundle\Aop\PointcutInterface;

class LoggingPointcut implements PointcutInterface
{
    public function matchesClass(\ReflectionClass $class)
    {
        return true;
    }

    /**
     * Catches all API calls using AOP point cut
     * @param \ReflectionMethod $method
     * @return bool
     */
    public function matchesMethod(\ReflectionMethod $method)
    {
        $found = preg_match("/login|(update|get)Object/", $method->name);
        return $found;
    }
}