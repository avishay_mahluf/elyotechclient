<?php

namespace AppBundle\Log;

use CG\Proxy\MethodInterceptorInterface;
use CG\Proxy\MethodInvocation;
use Psr\Log\LoggerInterface;
use Symfony\Component\Serializer\Encoder\JsonEncoder;

class LoggingInterceptor implements MethodInterceptorInterface
{
    private $logger;

    /**
     * @param LoggerInterface $logger Injected logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * Log API activity using injected logger
     * @param MethodInvocation $invocation
     * @return mixed
     */
    public function intercept(MethodInvocation $invocation)
    {
        $encoder = new JsonEncoder();

        // log message to log file
        switch ($invocation->reflection->name) {
            case 'login':
                $this->logger->info(sprintf('API login requested for username: "%s".', $invocation->getNamedArgument('userName')));
                break;
            case 'getObjects':
                $this->logger->info(sprintf('API get requested for "%s".', $invocation->getNamedArgument('relativeUrl')));
                break;
            case 'updateObject':
                $objectJson = $encoder->encode($invocation->getNamedArgument('objectData'), JsonEncoder::FORMAT);
                $this->logger->info(sprintf('API put request for "%s".', $invocation->getNamedArgument('relativeUrl')), array($objectJson));
                break;

        }

        // proceed to method
        return $invocation->proceed();
    }
}