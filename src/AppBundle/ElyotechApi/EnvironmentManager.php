<?php

namespace AppBundle\ElyotechApi;

class EnvironmentManager
{
    protected $apiConnector;

    /**
     * @param ElyotechApiConnector $apiConnector an Elyotech api connector service as a dependency
     */
    public function __construct(ElyotechApiConnector $apiConnector)
    {
        $this->apiConnector = $apiConnector;
    }

    /**
     * Get list of environments
     * @param string $apiToken API token for the user session
     * @return mixed list of objects or null if didn't succeeded
     */
    public function getEnvironments($apiToken)
    {
        $relativeUrl = 'api/admin/environment';
        return $this->apiConnector->getObjects($relativeUrl, $apiToken);
    }

    /**
     * Get a single environment by ID
     * Note - API does not have single environment get request so need to filter it out
     * @param string $apiToken API token for the user session
     * @param int $environmentId ID of the desired environment
     * @return mixed environment object or null if didn't succeeded
     */
    public function getEnvironmentById($apiToken, $environmentId)
    {
        $environments = $this->getEnvironments($apiToken);

        if ($environments && empty($environments->error)) {
            foreach ($environments as $environment) {
                if ($environment->id == $environmentId) {
                    return $environment;
                }
            }
        }

        return null;
    }

    /**
     * Update Environment object through API
     * @param \stdClass $envObject the updated object as associative array
     * @param string $apiToken API token for the user session
     * @return mixed the updated object or null if no success
     */
    public function updateEnvironment(\stdClass $envObject, $apiToken)
    {
        // build url call for environments
        $relativeUrl = 'api/admin/environment/' . $envObject->id;

        $envObject = clone $envObject;
        // remove the unneeded id from object
        unset($envObject->id);

        // set up params for api call
        $rawData = (object)array(
            'environment' => $envObject,
        );

        // update the object
        $response = $this->apiConnector->updateObject($relativeUrl, $apiToken, $rawData);

        return $response;
    }

    /**
     * Update single field on an environment
     * @param $apiToken
     * @param $environmentId
     * @param $fieldName
     * @param $change
     * @return mixed|null
     */
    public function updateSingleField($apiToken, $environmentId, $fieldName, $change)
    {
        // get the object
        $environmentObj = $this->getEnvironmentById($apiToken, $environmentId);

        if (!empty($environmentObj)) {
            // update field
            $environmentObj->$fieldName = $change;
            // update the environment
            $response = $this->updateEnvironment($environmentObj, $apiToken);

            return $response;
        }

        return null;
    }
}