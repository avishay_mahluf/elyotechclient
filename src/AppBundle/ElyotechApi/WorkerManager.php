<?php

namespace AppBundle\ElyotechApi;

class WorkerManager
{
    protected $apiConnector;

    /**
     * @param ElyotechApiConnector $apiConnector an Elyotech api connector service as a dependency
     */
    public function __construct(ElyotechApiConnector $apiConnector)
    {
        $this->apiConnector = $apiConnector;
    }

    /**
     * Get list of workers
     * @param string $apiToken API token for the user session
     * @return string|null list of objects or null if didn't succeeded
     */
    public function getWorkers($apiToken)
    {
        $relativeUrl = 'api/admin/workers/active';
        return $this->apiConnector->getObjects($relativeUrl, $apiToken);
    }
}