<?php

namespace AppBundle\ElyotechApi;

use Lsw\ApiCallerBundle\Call\HttpPutJson;
use Lsw\ApiCallerBundle\Caller\LoggingApiCaller;
use Lsw\ApiCallerBundle\Call\HttpGetJson;

class ElyotechApiConnector
{
    const ELYOTECH_BASE_HOST = 'http://devops-sf.travelyo.com/';

    protected $apiCaller;

    /**
     * @param LoggingApiCaller $apiCaller Api caller service as a dependency
     */
    public function __construct(LoggingApiCaller $apiCaller)
    {
        $this->apiCaller = $apiCaller;
    }

    /**
     * Perform API login call
     * @param string $relativeUrl relative URL for the API call
     * @param string $userName the user name
     * @param string $password the password
     * @return mixed json with session token or error on failure
     */
    public function login($relativeUrl, $userName, $password)
    {
        // build url
        $connectionLink = self::ELYOTECH_BASE_HOST . $relativeUrl;

        // set up params for api call
        $parameters = (object)array(
            'password' => $password,
            'username' => $userName,
        );

        // execute api login call
        $response = $this->apiCaller->call(new HttpGetJson($connectionLink, $parameters));

        return $response;
    }

    /**
     * Get list of object from API
     * @param string $relativeUrl relative URL for the API call
     * @param string $apiToken API token for the user session
     * @return string|null list of objects or null if didn't succeeded
     */
    public function getObjects($relativeUrl, $apiToken)
    {
        // build url
        $connectionLink = self::ELYOTECH_BASE_HOST . $relativeUrl;

        // set up params for api call
        $parameters = (object)array(
            'access_token' => $apiToken,
        );

        // execute api call
        $response = $this->apiCaller->call(new HttpGetJson($connectionLink, $parameters));

        return $response;
    }

    /**
     * Update an object using PUT request to API
     * @param string $relativeUrl relative URL for the API call
     * @param string $apiToken API token for the user session
     * @param \stdClass $objectData the updated object
     * @return mixed the updated object or null if no success
     */
    public function updateObject($relativeUrl, $apiToken, \stdClass $objectData)
    {
        // build url
        $connectionLink = self::ELYOTECH_BASE_HOST . $relativeUrl . '?access_token=' . $apiToken;
        // execute api call
        $response = $this->apiCaller->call(new HttpPutJson($connectionLink, $objectData));

        return $response;
    }

}