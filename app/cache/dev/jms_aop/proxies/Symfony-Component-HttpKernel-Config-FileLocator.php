<?php

namespace EnhancedProxyf57672ad_3732d3300d9ae14edf92160a0a310ccc6d0804c6\__CG__\Symfony\Component\HttpKernel\Config;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class FileLocator extends \Symfony\Component\HttpKernel\Config\FileLocator
{
    private $__CGInterception__loader;

    public function locate($file, $currentPath = NULL, $first = true)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Config\\FileLocator', 'locate');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($file, $currentPath, $first));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($file, $currentPath, $first), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}