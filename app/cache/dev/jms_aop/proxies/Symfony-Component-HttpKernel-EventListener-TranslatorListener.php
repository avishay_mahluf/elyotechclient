<?php

namespace EnhancedProxyf57672ad_b91c8121e5eac82b458951b5837ff0d0928d79dd\__CG__\Symfony\Component\HttpKernel\EventListener;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class TranslatorListener extends \Symfony\Component\HttpKernel\EventListener\TranslatorListener
{
    private $__CGInterception__loader;

    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\EventListener\\TranslatorListener', 'onKernelRequest');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($event));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($event), $interceptors);

        return $invocation->proceed();
    }

    public function onKernelFinishRequest(\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\EventListener\\TranslatorListener', 'onKernelFinishRequest');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($event));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($event), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}