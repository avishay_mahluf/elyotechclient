<?php

namespace EnhancedProxyf57672ad_b9498a58041b2b5ccd2379478485bb1154f9e42a\__CG__\Symfony\Bundle\WebProfilerBundle\Controller;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class ExceptionController extends \Symfony\Bundle\WebProfilerBundle\Controller\ExceptionController
{
    private $__CGInterception__loader;

    public function showAction($token)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\ExceptionController', 'showAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($token));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($token), $interceptors);

        return $invocation->proceed();
    }

    public function cssAction($token)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\ExceptionController', 'cssAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($token));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($token), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}