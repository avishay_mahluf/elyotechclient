<?php

namespace EnhancedProxyf57672ad_1f7e4ad32caf570c6fe3b01b8504e5e7272e1c42\__CG__\Symfony\Component\Translation\Loader;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class PhpFileLoader extends \Symfony\Component\Translation\Loader\PhpFileLoader
{
    private $__CGInterception__loader;

    public function load($resource, $locale, $domain = 'messages')
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Loader\\PhpFileLoader', 'load');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($resource, $locale, $domain));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($resource, $locale, $domain), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}