<?php

namespace EnhancedProxyf57672ad_773e7b8b6ce66c754c0875cf86c1ef985ecdd6ba\__CG__\Symfony\Component\HttpKernel\EventListener;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class LocaleListener extends \Symfony\Component\HttpKernel\EventListener\LocaleListener
{
    private $__CGInterception__loader;

    public function setRequest(\Symfony\Component\HttpFoundation\Request $request = NULL)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\EventListener\\LocaleListener', 'setRequest');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request), $interceptors);

        return $invocation->proceed();
    }

    public function onKernelRequest(\Symfony\Component\HttpKernel\Event\GetResponseEvent $event)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\EventListener\\LocaleListener', 'onKernelRequest');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($event));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($event), $interceptors);

        return $invocation->proceed();
    }

    public function onKernelFinishRequest(\Symfony\Component\HttpKernel\Event\FinishRequestEvent $event)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\EventListener\\LocaleListener', 'onKernelFinishRequest');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($event));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($event), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}