<?php

namespace EnhancedProxyf57672ad_ad1cf06d26b14ee4cc2c1c4cf9ba36c50e2f5690\__CG__\Symfony\Component\HttpKernel\EventListener;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class StreamedResponseListener extends \Symfony\Component\HttpKernel\EventListener\StreamedResponseListener
{
    private $__CGInterception__loader;

    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\EventListener\\StreamedResponseListener', 'onKernelResponse');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($event));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($event), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}