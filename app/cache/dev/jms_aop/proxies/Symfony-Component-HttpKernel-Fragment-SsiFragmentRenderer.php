<?php

namespace EnhancedProxyf57672ad_58b73bf0b3c10aeaea335a8b3eac71e2765bfdb1\__CG__\Symfony\Component\HttpKernel\Fragment;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class SsiFragmentRenderer extends \Symfony\Component\HttpKernel\Fragment\SsiFragmentRenderer
{
    private $__CGInterception__loader;

    public function setFragmentPath($path)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\RoutableFragmentRenderer', 'setFragmentPath');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($path));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($path), $interceptors);

        return $invocation->proceed();
    }

    public function render($uri, \Symfony\Component\HttpFoundation\Request $request, array $options = array())
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\AbstractSurrogateFragmentRenderer', 'render');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($uri, $request, $options));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($uri, $request, $options), $interceptors);

        return $invocation->proceed();
    }

    public function getName()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\SsiFragmentRenderer', 'getName');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }

    protected function generateFragmentUri(\Symfony\Component\HttpKernel\Controller\ControllerReference $reference, \Symfony\Component\HttpFoundation\Request $request, $absolute = false, $strict = true)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\RoutableFragmentRenderer', 'generateFragmentUri');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($reference, $request, $absolute, $strict));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($reference, $request, $absolute, $strict), $interceptors);

        return $invocation->proceed();
    }
}