<?php

namespace EnhancedProxyf57672ad_47d49609b637013cf8066ad7dc8e6f3445326f30\__CG__\AppBundle\Aop;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class AuditInterceptor extends \AppBundle\Aop\AuditInterceptor
{
    private $__CGInterception__loader;

    public function intercept(\CG\Proxy\MethodInvocation $invocation)
    {
        $ref = new \ReflectionMethod('AppBundle\\Log\\AuditInterceptor', 'intercept');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($invocation));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($invocation), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}