<?php

namespace EnhancedProxyf57672ad_aa3b17a06c54069051111edd177f4b93a6b0ac40\__CG__\Symfony\Bundle\WebProfilerBundle\Controller;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class ProfilerController extends \Symfony\Bundle\WebProfilerBundle\Controller\ProfilerController
{
    private $__CGInterception__loader;

    public function toolbarAction(\Symfony\Component\HttpFoundation\Request $request, $token)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController', 'toolbarAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request, $token));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request, $token), $interceptors);

        return $invocation->proceed();
    }

    public function searchResultsAction(\Symfony\Component\HttpFoundation\Request $request, $token)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController', 'searchResultsAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request, $token));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request, $token), $interceptors);

        return $invocation->proceed();
    }

    public function searchBarAction(\Symfony\Component\HttpFoundation\Request $request)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController', 'searchBarAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request), $interceptors);

        return $invocation->proceed();
    }

    public function searchAction(\Symfony\Component\HttpFoundation\Request $request)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController', 'searchAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request), $interceptors);

        return $invocation->proceed();
    }

    public function purgeAction()
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController', 'purgeAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function phpinfoAction()
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController', 'phpinfoAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function panelAction(\Symfony\Component\HttpFoundation\Request $request, $token)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController', 'panelAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request, $token));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request, $token), $interceptors);

        return $invocation->proceed();
    }

    public function infoAction($about)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController', 'infoAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($about));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($about), $interceptors);

        return $invocation->proceed();
    }

    public function homeAction()
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\ProfilerController', 'homeAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}