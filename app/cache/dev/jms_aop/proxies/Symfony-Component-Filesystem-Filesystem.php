<?php

namespace EnhancedProxyf57672ad_ccaacc604d1a32fbae121847476d56f04847bddc\__CG__\Symfony\Component\Filesystem;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class Filesystem extends \Symfony\Component\Filesystem\Filesystem
{
    private $__CGInterception__loader;

    public function touch($files, $time = NULL, $atime = NULL)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'touch');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($files, $time, $atime));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($files, $time, $atime), $interceptors);

        return $invocation->proceed();
    }

    public function symlink($originDir, $targetDir, $copyOnWindows = false)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'symlink');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($originDir, $targetDir, $copyOnWindows));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($originDir, $targetDir, $copyOnWindows), $interceptors);

        return $invocation->proceed();
    }

    public function rename($origin, $target, $overwrite = false)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'rename');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($origin, $target, $overwrite));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($origin, $target, $overwrite), $interceptors);

        return $invocation->proceed();
    }

    public function remove($files)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'remove');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($files));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($files), $interceptors);

        return $invocation->proceed();
    }

    public function mkdir($dirs, $mode = 511)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'mkdir');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($dirs, $mode));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($dirs, $mode), $interceptors);

        return $invocation->proceed();
    }

    public function mirror($originDir, $targetDir, \Traversable $iterator = NULL, $options = array())
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'mirror');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($originDir, $targetDir, $iterator, $options));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($originDir, $targetDir, $iterator, $options), $interceptors);

        return $invocation->proceed();
    }

    public function makePathRelative($endPath, $startPath)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'makePathRelative');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($endPath, $startPath));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($endPath, $startPath), $interceptors);

        return $invocation->proceed();
    }

    public function isAbsolutePath($file)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'isAbsolutePath');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($file));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($file), $interceptors);

        return $invocation->proceed();
    }

    public function exists($files)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'exists');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($files));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($files), $interceptors);

        return $invocation->proceed();
    }

    public function dumpFile($filename, $content, $mode = 438)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'dumpFile');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($filename, $content, $mode));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($filename, $content, $mode), $interceptors);

        return $invocation->proceed();
    }

    public function copy($originFile, $targetFile, $override = false)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'copy');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($originFile, $targetFile, $override));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($originFile, $targetFile, $override), $interceptors);

        return $invocation->proceed();
    }

    public function chown($files, $user, $recursive = false)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'chown');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($files, $user, $recursive));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($files, $user, $recursive), $interceptors);

        return $invocation->proceed();
    }

    public function chmod($files, $mode, $umask = 0, $recursive = false)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'chmod');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($files, $mode, $umask, $recursive));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($files, $mode, $umask, $recursive), $interceptors);

        return $invocation->proceed();
    }

    public function chgrp($files, $group, $recursive = false)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Filesystem\\Filesystem', 'chgrp');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($files, $group, $recursive));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($files, $group, $recursive), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}