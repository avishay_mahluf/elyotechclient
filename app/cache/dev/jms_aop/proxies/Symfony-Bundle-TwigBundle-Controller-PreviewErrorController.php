<?php

namespace EnhancedProxyf57672ad_11b89a4e461f01ed37b31ce14113cee2c84ab44d\__CG__\Symfony\Bundle\TwigBundle\Controller;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class PreviewErrorController extends \Symfony\Bundle\TwigBundle\Controller\PreviewErrorController
{
    private $__CGInterception__loader;

    public function previewErrorPageAction(\Symfony\Component\HttpFoundation\Request $request, $code)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\TwigBundle\\Controller\\PreviewErrorController', 'previewErrorPageAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request, $code));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request, $code), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}