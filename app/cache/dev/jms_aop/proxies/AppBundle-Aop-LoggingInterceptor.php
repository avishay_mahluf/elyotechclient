<?php

namespace EnhancedProxyf57672ad_44f60d9b43a7575c320467583f809475f6392ac2\__CG__\AppBundle\Aop;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class LoggingInterceptor extends \AppBundle\Aop\LoggingInterceptor
{
    private $__CGInterception__loader;

    public function intercept(\CG\Proxy\MethodInvocation $invocation)
    {
        $ref = new \ReflectionMethod('AppBundle\\Log\\LoggingInterceptor', 'intercept');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($invocation));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($invocation), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}