<?php

namespace EnhancedProxyf57672ad_739ebfd0dc19f99598bef2ad782d13e90cf8f37a\__CG__\Symfony\Component\HttpKernel;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class UriSigner extends \Symfony\Component\HttpKernel\UriSigner
{
    private $__CGInterception__loader;

    public function sign($uri)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\UriSigner', 'sign');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($uri));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($uri), $interceptors);

        return $invocation->proceed();
    }

    public function check($uri)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\UriSigner', 'check');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($uri));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($uri), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}