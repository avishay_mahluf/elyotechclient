<?php

namespace EnhancedProxyf57672ad_c57955b530b776ef1a7bf249ae1ae3179159eace\__CG__\Symfony\Bundle\FrameworkBundle\CacheWarmer;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class TranslationsCacheWarmer extends \Symfony\Bundle\FrameworkBundle\CacheWarmer\TranslationsCacheWarmer
{
    private $__CGInterception__loader;

    public function warmUp($cacheDir)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\FrameworkBundle\\CacheWarmer\\TranslationsCacheWarmer', 'warmUp');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($cacheDir));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($cacheDir), $interceptors);

        return $invocation->proceed();
    }

    public function isOptional()
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\FrameworkBundle\\CacheWarmer\\TranslationsCacheWarmer', 'isOptional');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}