<?php

namespace EnhancedProxyf57672ad_7bd615aecb8ad8b12970036dcc398b69ee00a2e0\__CG__\AppBundle\ElyotechApi;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class EnvironmentManager extends \AppBundle\ElyotechApi\EnvironmentManager
{
    private $__CGInterception__loader;

    public function updateEnvironment($envObject, $apiToken)
    {
        $ref = new \ReflectionMethod('AppBundle\\ElyotechApi\\EnvironmentManager', 'updateEnvironment');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($envObject, $apiToken));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($envObject, $apiToken), $interceptors);

        return $invocation->proceed();
    }

    public function getEnvironments($apiToken)
    {
        $ref = new \ReflectionMethod('AppBundle\\ElyotechApi\\EnvironmentManager', 'getEnvironments');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($apiToken));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($apiToken), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}