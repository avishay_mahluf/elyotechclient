<?php

namespace EnhancedProxyf57672ad_9a69f706b38450cd84cf0c2984538a2c0eda7c16\__CG__\Symfony\Component\HttpKernel\EventListener;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class ResponseListener extends \Symfony\Component\HttpKernel\EventListener\ResponseListener
{
    private $__CGInterception__loader;

    public function onKernelResponse(\Symfony\Component\HttpKernel\Event\FilterResponseEvent $event)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\EventListener\\ResponseListener', 'onKernelResponse');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($event));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($event), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}