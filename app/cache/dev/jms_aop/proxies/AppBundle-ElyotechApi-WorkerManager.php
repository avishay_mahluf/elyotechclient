<?php

namespace EnhancedProxyf57672ad_292a7b283ba15e15f239d50bf13345cd5dca404a\__CG__\AppBundle\ElyotechApi;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class WorkerManager extends \AppBundle\ElyotechApi\WorkerManager
{
    private $__CGInterception__loader;

    public function getWorkers($apiToken)
    {
        $ref = new \ReflectionMethod('AppBundle\\ElyotechApi\\WorkerManager', 'getWorkers');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($apiToken));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($apiToken), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}