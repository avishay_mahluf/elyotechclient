<?php

namespace EnhancedProxy9332699b_96a64bdcfce14009c1fb666d9dace4e29504f14b\__CG__\AppBundle\ElyotechApi;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class ElyotechApiConnector extends \AppBundle\ElyotechApi\ElyotechApiConnector
{
    private $__CGInterception__loader;

    public function updateObject($relativeUrl, $apiToken, \stdClass $objectData)
    {
        $ref = new \ReflectionMethod('AppBundle\\ElyotechApi\\ElyotechApiConnector', 'updateObject');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($relativeUrl, $apiToken, $objectData));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($relativeUrl, $apiToken, $objectData), $interceptors);

        return $invocation->proceed();
    }

    public function login($relativeUrl, $userName, $password)
    {
        $ref = new \ReflectionMethod('AppBundle\\ElyotechApi\\ElyotechApiConnector', 'login');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($relativeUrl, $userName, $password));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($relativeUrl, $userName, $password), $interceptors);

        return $invocation->proceed();
    }

    public function getObjects($relativeUrl, $apiToken)
    {
        $ref = new \ReflectionMethod('AppBundle\\ElyotechApi\\ElyotechApiConnector', 'getObjects');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($relativeUrl, $apiToken));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($relativeUrl, $apiToken), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}