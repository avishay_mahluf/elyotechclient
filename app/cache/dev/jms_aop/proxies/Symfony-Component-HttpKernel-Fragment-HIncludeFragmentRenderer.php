<?php

namespace EnhancedProxyf57672ad_d14f0969b840f4ed8d857d6e51b67c06fe786e1b\__CG__\Symfony\Component\HttpKernel\Fragment;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class HIncludeFragmentRenderer extends \Symfony\Component\HttpKernel\Fragment\HIncludeFragmentRenderer
{
    private $__CGInterception__loader;

    public function setTemplating($templating)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\HIncludeFragmentRenderer', 'setTemplating');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($templating));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($templating), $interceptors);

        return $invocation->proceed();
    }

    public function setFragmentPath($path)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\RoutableFragmentRenderer', 'setFragmentPath');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($path));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($path), $interceptors);

        return $invocation->proceed();
    }

    public function render($uri, \Symfony\Component\HttpFoundation\Request $request, array $options = array())
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\HIncludeFragmentRenderer', 'render');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($uri, $request, $options));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($uri, $request, $options), $interceptors);

        return $invocation->proceed();
    }

    public function hasTemplating()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\HIncludeFragmentRenderer', 'hasTemplating');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getName()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\HIncludeFragmentRenderer', 'getName');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }

    protected function generateFragmentUri(\Symfony\Component\HttpKernel\Controller\ControllerReference $reference, \Symfony\Component\HttpFoundation\Request $request, $absolute = false, $strict = true)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\RoutableFragmentRenderer', 'generateFragmentUri');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($reference, $request, $absolute, $strict));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($reference, $request, $absolute, $strict), $interceptors);

        return $invocation->proceed();
    }
}