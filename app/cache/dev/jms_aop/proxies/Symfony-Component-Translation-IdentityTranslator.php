<?php

namespace EnhancedProxyf57672ad_17ba2b0d668876b2000de5e34b43642052fe27c9\__CG__\Symfony\Component\Translation;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class IdentityTranslator extends \Symfony\Component\Translation\IdentityTranslator
{
    private $__CGInterception__loader;

    public function transChoice($id, $number, array $parameters = array(), $domain = NULL, $locale = NULL)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\IdentityTranslator', 'transChoice');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($id, $number, $parameters, $domain, $locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($id, $number, $parameters, $domain, $locale), $interceptors);

        return $invocation->proceed();
    }

    public function trans($id, array $parameters = array(), $domain = NULL, $locale = NULL)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\IdentityTranslator', 'trans');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($id, $parameters, $domain, $locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($id, $parameters, $domain, $locale), $interceptors);

        return $invocation->proceed();
    }

    public function setLocale($locale)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\IdentityTranslator', 'setLocale');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($locale), $interceptors);

        return $invocation->proceed();
    }

    public function getLocale()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\IdentityTranslator', 'getLocale');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}