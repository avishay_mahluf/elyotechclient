<?php

namespace EnhancedProxyf57672ad_518f06a75425b7f6870467f1019ff94c930a0f5d\__CG__\AppBundle\Twig;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class ToArrayExtension extends \AppBundle\Twig\ToArrayExtension
{
    private $__CGInterception__loader;

    public function to_array($object)
    {
        $ref = new \ReflectionMethod('AppBundle\\Twig\\ToArrayExtension', 'to_array');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($object));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($object), $interceptors);

        return $invocation->proceed();
    }

    public function initRuntime(\Twig_Environment $environment)
    {
        $ref = new \ReflectionMethod('Twig_Extension', 'initRuntime');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($environment));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($environment), $interceptors);

        return $invocation->proceed();
    }

    public function getTokenParsers()
    {
        $ref = new \ReflectionMethod('Twig_Extension', 'getTokenParsers');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getTests()
    {
        $ref = new \ReflectionMethod('Twig_Extension', 'getTests');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getOperators()
    {
        $ref = new \ReflectionMethod('Twig_Extension', 'getOperators');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getNodeVisitors()
    {
        $ref = new \ReflectionMethod('Twig_Extension', 'getNodeVisitors');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getName()
    {
        $ref = new \ReflectionMethod('AppBundle\\Twig\\ToArrayExtension', 'getName');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getGlobals()
    {
        $ref = new \ReflectionMethod('Twig_Extension', 'getGlobals');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getFunctions()
    {
        $ref = new \ReflectionMethod('Twig_Extension', 'getFunctions');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getFilters()
    {
        $ref = new \ReflectionMethod('AppBundle\\Twig\\ToArrayExtension', 'getFilters');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}