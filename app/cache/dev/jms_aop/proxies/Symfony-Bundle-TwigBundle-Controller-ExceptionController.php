<?php

namespace EnhancedProxyf57672ad_597877fc1359850fa8786ead82dcc8f6f491d628\__CG__\Symfony\Bundle\TwigBundle\Controller;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class ExceptionController extends \Symfony\Bundle\TwigBundle\Controller\ExceptionController
{
    private $__CGInterception__loader;

    public function showAction(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpKernel\Exception\FlattenException $exception, \Symfony\Component\HttpKernel\Log\DebugLoggerInterface $logger = NULL)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\TwigBundle\\Controller\\ExceptionController', 'showAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request, $exception, $logger));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request, $exception, $logger), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}