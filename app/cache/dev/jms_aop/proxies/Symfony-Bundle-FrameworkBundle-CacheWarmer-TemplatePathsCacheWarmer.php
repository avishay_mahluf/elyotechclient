<?php

namespace EnhancedProxyf57672ad_31a4653c2fec3897ee92bdf9e610a4df42d2a58a\__CG__\Symfony\Bundle\FrameworkBundle\CacheWarmer;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class TemplatePathsCacheWarmer extends \Symfony\Bundle\FrameworkBundle\CacheWarmer\TemplatePathsCacheWarmer
{
    private $__CGInterception__loader;

    public function warmUp($cacheDir)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\FrameworkBundle\\CacheWarmer\\TemplatePathsCacheWarmer', 'warmUp');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($cacheDir));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($cacheDir), $interceptors);

        return $invocation->proceed();
    }

    public function isOptional()
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\FrameworkBundle\\CacheWarmer\\TemplatePathsCacheWarmer', 'isOptional');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }

    protected function writeCacheFile($file, $content)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\CacheWarmer\\CacheWarmer', 'writeCacheFile');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($file, $content));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($file, $content), $interceptors);

        return $invocation->proceed();
    }
}