<?php

namespace EnhancedProxyf57672ad_fc72b8512010578c1d513eb549cae737011d27b2\__CG__\Symfony\Component\Translation\Loader;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class CsvFileLoader extends \Symfony\Component\Translation\Loader\CsvFileLoader
{
    private $__CGInterception__loader;

    public function setCsvControl($delimiter = ';', $enclosure = '"', $escape = '\\')
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Loader\\CsvFileLoader', 'setCsvControl');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($delimiter, $enclosure, $escape));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($delimiter, $enclosure, $escape), $interceptors);

        return $invocation->proceed();
    }

    public function load($resource, $locale, $domain = 'messages')
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Loader\\CsvFileLoader', 'load');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($resource, $locale, $domain));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($resource, $locale, $domain), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}