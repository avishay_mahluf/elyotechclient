<?php

namespace EnhancedProxyf57672ad_713e46860f3d9fc3b995e8f98685994cc9433d79\__CG__\Symfony\Component\HttpKernel\DependencyInjection;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class LazyLoadingFragmentHandler extends \Symfony\Component\HttpKernel\DependencyInjection\LazyLoadingFragmentHandler
{
    private $__CGInterception__loader;

    public function setRequest(\Symfony\Component\HttpFoundation\Request $request = NULL)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\FragmentHandler', 'setRequest');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request), $interceptors);

        return $invocation->proceed();
    }

    public function render($uri, $renderer = 'inline', array $options = array())
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\DependencyInjection\\LazyLoadingFragmentHandler', 'render');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($uri, $renderer, $options));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($uri, $renderer, $options), $interceptors);

        return $invocation->proceed();
    }

    public function addRendererService($name, $renderer)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\DependencyInjection\\LazyLoadingFragmentHandler', 'addRendererService');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($name, $renderer));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($name, $renderer), $interceptors);

        return $invocation->proceed();
    }

    public function addRenderer(\Symfony\Component\HttpKernel\Fragment\FragmentRendererInterface $renderer)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\FragmentHandler', 'addRenderer');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($renderer));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($renderer), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }

    protected function deliver(\Symfony\Component\HttpFoundation\Response $response)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\Fragment\\FragmentHandler', 'deliver');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($response));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($response), $interceptors);

        return $invocation->proceed();
    }
}