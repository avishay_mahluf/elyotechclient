<?php

namespace EnhancedProxyf57672ad_cd25e053096ab57ac64862391e8f4a27e4c926db\__CG__\AppBundle\Aop;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class AuditPointcut extends \AppBundle\Aop\AuditPointcut
{
    private $__CGInterception__loader;

    public function matchesMethod(\ReflectionMethod $method)
    {
        $ref = new \ReflectionMethod('AppBundle\\Log\\AuditPointcut', 'matchesMethod');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($method));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($method), $interceptors);

        return $invocation->proceed();
    }

    public function matchesClass(\ReflectionClass $class)
    {
        $ref = new \ReflectionMethod('AppBundle\\Log\\AuditPointcut', 'matchesClass');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($class));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($class), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}