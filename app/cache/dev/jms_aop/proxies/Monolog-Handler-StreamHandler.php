<?php

namespace EnhancedProxyf57672ad_d287ac28ad120b605e9e7f0cbd05d7062fe5e0ee\__CG__\Monolog\Handler;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class StreamHandler extends \Monolog\Handler\StreamHandler
{
    private $__CGInterception__loader;

    public function setLevel($level)
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', 'setLevel');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($level));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($level), $interceptors);

        return $invocation->proceed();
    }

    public function setFormatter(\Monolog\Formatter\FormatterInterface $formatter)
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', 'setFormatter');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($formatter));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($formatter), $interceptors);

        return $invocation->proceed();
    }

    public function setBubble($bubble)
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', 'setBubble');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($bubble));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($bubble), $interceptors);

        return $invocation->proceed();
    }

    public function pushProcessor($callback)
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', 'pushProcessor');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($callback));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($callback), $interceptors);

        return $invocation->proceed();
    }

    public function popProcessor()
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', 'popProcessor');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function isHandling(array $record)
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', 'isHandling');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($record));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($record), $interceptors);

        return $invocation->proceed();
    }

    public function handleBatch(array $records)
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', 'handleBatch');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($records));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($records), $interceptors);

        return $invocation->proceed();
    }

    public function handle(array $record)
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractProcessingHandler', 'handle');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($record));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($record), $interceptors);

        return $invocation->proceed();
    }

    public function getLevel()
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', 'getLevel');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getFormatter()
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', 'getFormatter');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getBubble()
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', 'getBubble');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function close()
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\StreamHandler', 'close');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __destruct()
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', '__destruct');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }

    protected function write(array $record)
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\StreamHandler', 'write');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($record));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($record), $interceptors);

        return $invocation->proceed();
    }

    protected function processRecord(array $record)
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractProcessingHandler', 'processRecord');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($record));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($record), $interceptors);

        return $invocation->proceed();
    }

    protected function getDefaultFormatter()
    {
        $ref = new \ReflectionMethod('Monolog\\Handler\\AbstractHandler', 'getDefaultFormatter');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }
}