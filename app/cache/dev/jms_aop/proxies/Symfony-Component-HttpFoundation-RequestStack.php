<?php

namespace EnhancedProxyf57672ad_94b81467661afc7915f54af7d59db8801b6d8cea\__CG__\Symfony\Component\HttpFoundation;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class RequestStack extends \Symfony\Component\HttpFoundation\RequestStack
{
    private $__CGInterception__loader;

    public function push(\Symfony\Component\HttpFoundation\Request $request)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpFoundation\\RequestStack', 'push');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request), $interceptors);

        return $invocation->proceed();
    }

    public function pop()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpFoundation\\RequestStack', 'pop');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getParentRequest()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpFoundation\\RequestStack', 'getParentRequest');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getMasterRequest()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpFoundation\\RequestStack', 'getMasterRequest');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getCurrentRequest()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpFoundation\\RequestStack', 'getCurrentRequest');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}