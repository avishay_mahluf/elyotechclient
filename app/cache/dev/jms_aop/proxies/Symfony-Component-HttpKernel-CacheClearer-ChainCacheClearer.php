<?php

namespace EnhancedProxyf57672ad_f80d8881988b3a00e00e219346d7b4448cccf24f\__CG__\Symfony\Component\HttpKernel\CacheClearer;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class ChainCacheClearer extends \Symfony\Component\HttpKernel\CacheClearer\ChainCacheClearer
{
    private $__CGInterception__loader;

    public function clear($cacheDir)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\CacheClearer\\ChainCacheClearer', 'clear');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($cacheDir));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($cacheDir), $interceptors);

        return $invocation->proceed();
    }

    public function add(\Symfony\Component\HttpKernel\CacheClearer\CacheClearerInterface $clearer)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\CacheClearer\\ChainCacheClearer', 'add');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($clearer));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($clearer), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}