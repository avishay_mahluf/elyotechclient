<?php

namespace EnhancedProxyf57672ad_a68b15ee257d20e5f8545c1d74c8cc43f9b3287b\__CG__\Symfony\Component\Translation;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class MessageSelector extends \Symfony\Component\Translation\MessageSelector
{
    private $__CGInterception__loader;

    public function choose($message, $number, $locale)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\MessageSelector', 'choose');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($message, $number, $locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($message, $number, $locale), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}