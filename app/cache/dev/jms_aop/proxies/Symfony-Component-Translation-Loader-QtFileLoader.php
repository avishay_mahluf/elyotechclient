<?php

namespace EnhancedProxyf57672ad_4fe077c31c97e871d2c8bba640a669817df876e9\__CG__\Symfony\Component\Translation\Loader;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class QtFileLoader extends \Symfony\Component\Translation\Loader\QtFileLoader
{
    private $__CGInterception__loader;

    public function load($resource, $locale, $domain = 'messages')
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Loader\\QtFileLoader', 'load');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($resource, $locale, $domain));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($resource, $locale, $domain), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}