<?php

namespace EnhancedProxyf57672ad_2a9e6f39a2137a727fe4b5b3e4ad6c42c40c322f\__CG__\Symfony\Component\HttpKernel\DependencyInjection;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class ContainerAwareHttpKernel extends \Symfony\Component\HttpKernel\DependencyInjection\ContainerAwareHttpKernel
{
    private $__CGInterception__loader;

    public function terminateWithException(\Exception $exception)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\HttpKernel', 'terminateWithException');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($exception));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($exception), $interceptors);

        return $invocation->proceed();
    }

    public function terminate(\Symfony\Component\HttpFoundation\Request $request, \Symfony\Component\HttpFoundation\Response $response)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\HttpKernel', 'terminate');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request, $response));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request, $response), $interceptors);

        return $invocation->proceed();
    }

    public function handle(\Symfony\Component\HttpFoundation\Request $request, $type = 1, $catch = true)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\DependencyInjection\\ContainerAwareHttpKernel', 'handle');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request, $type, $catch));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request, $type, $catch), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}