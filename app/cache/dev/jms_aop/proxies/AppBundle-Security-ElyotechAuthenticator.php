<?php

namespace EnhancedProxyf57672ad_102726c635132f2764edd4189ca2a69c4c7e3831\__CG__\AppBundle\Security;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class ElyotechAuthenticator extends \AppBundle\Security\ElyotechAuthenticator
{
    private $__CGInterception__loader;

    public function supportsToken(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token, $providerKey)
    {
        $ref = new \ReflectionMethod('AppBundle\\Security\\ElyotechAuthenticator', 'supportsToken');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($token, $providerKey));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($token, $providerKey), $interceptors);

        return $invocation->proceed();
    }

    public function createToken(\Symfony\Component\HttpFoundation\Request $request, $username, $password, $providerKey)
    {
        $ref = new \ReflectionMethod('AppBundle\\Security\\ElyotechAuthenticator', 'createToken');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($request, $username, $password, $providerKey));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($request, $username, $password, $providerKey), $interceptors);

        return $invocation->proceed();
    }

    public function authenticateToken(\Symfony\Component\Security\Core\Authentication\Token\TokenInterface $token, \Symfony\Component\Security\Core\User\UserProviderInterface $userProvider, $providerKey)
    {
        $ref = new \ReflectionMethod('AppBundle\\Security\\ElyotechAuthenticator', 'authenticateToken');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($token, $userProvider, $providerKey));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($token, $userProvider, $providerKey), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}