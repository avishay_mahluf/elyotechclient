<?php

namespace EnhancedProxyf57672ad_8f16f0a3eb0abb21474507ef06b2c4f524d827a6\__CG__\Symfony\Bridge\Doctrine\CacheWarmer;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class ProxyCacheWarmer extends \Symfony\Bridge\Doctrine\CacheWarmer\ProxyCacheWarmer
{
    private $__CGInterception__loader;

    public function warmUp($cacheDir)
    {
        $ref = new \ReflectionMethod('Symfony\\Bridge\\Doctrine\\CacheWarmer\\ProxyCacheWarmer', 'warmUp');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($cacheDir));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($cacheDir), $interceptors);

        return $invocation->proceed();
    }

    public function isOptional()
    {
        $ref = new \ReflectionMethod('Symfony\\Bridge\\Doctrine\\CacheWarmer\\ProxyCacheWarmer', 'isOptional');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}