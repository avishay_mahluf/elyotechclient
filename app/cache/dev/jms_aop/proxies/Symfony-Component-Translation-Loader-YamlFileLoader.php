<?php

namespace EnhancedProxyf57672ad_6c28cbf34bbca023edef45e49094f4846164fd16\__CG__\Symfony\Component\Translation\Loader;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class YamlFileLoader extends \Symfony\Component\Translation\Loader\YamlFileLoader
{
    private $__CGInterception__loader;

    public function load($resource, $locale, $domain = 'messages')
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Loader\\YamlFileLoader', 'load');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($resource, $locale, $domain));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($resource, $locale, $domain), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}