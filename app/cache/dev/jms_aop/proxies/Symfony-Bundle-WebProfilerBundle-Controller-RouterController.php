<?php

namespace EnhancedProxyf57672ad_5afc36e3b826c76e915b338764680f2a03306e57\__CG__\Symfony\Bundle\WebProfilerBundle\Controller;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class RouterController extends \Symfony\Bundle\WebProfilerBundle\Controller\RouterController
{
    private $__CGInterception__loader;

    public function panelAction($token)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\WebProfilerBundle\\Controller\\RouterController', 'panelAction');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($token));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($token), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}