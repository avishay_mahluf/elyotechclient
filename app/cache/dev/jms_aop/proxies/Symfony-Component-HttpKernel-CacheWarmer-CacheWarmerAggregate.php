<?php

namespace EnhancedProxyf57672ad_46cae98504f7a5f69d68dfbdcf2c93f41efe949c\__CG__\Symfony\Component\HttpKernel\CacheWarmer;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class CacheWarmerAggregate extends \Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerAggregate
{
    private $__CGInterception__loader;

    public function warmUp($cacheDir)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\CacheWarmer\\CacheWarmerAggregate', 'warmUp');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($cacheDir));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($cacheDir), $interceptors);

        return $invocation->proceed();
    }

    public function setWarmers(array $warmers)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\CacheWarmer\\CacheWarmerAggregate', 'setWarmers');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($warmers));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($warmers), $interceptors);

        return $invocation->proceed();
    }

    public function isOptional()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\CacheWarmer\\CacheWarmerAggregate', 'isOptional');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function enableOptionalWarmers()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\CacheWarmer\\CacheWarmerAggregate', 'enableOptionalWarmers');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function add(\Symfony\Component\HttpKernel\CacheWarmer\CacheWarmerInterface $warmer)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\HttpKernel\\CacheWarmer\\CacheWarmerAggregate', 'add');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($warmer));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($warmer), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}