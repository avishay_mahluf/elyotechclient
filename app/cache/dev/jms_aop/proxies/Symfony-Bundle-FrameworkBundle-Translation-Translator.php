<?php

namespace EnhancedProxyf57672ad_7686325eee19ecc1d99f18916317af15124cdaf1\__CG__\Symfony\Bundle\FrameworkBundle\Translation;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class Translator extends \Symfony\Bundle\FrameworkBundle\Translation\Translator
{
    private $__CGInterception__loader;

    public function warmUp($cacheDir)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\FrameworkBundle\\Translation\\Translator', 'warmUp');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($cacheDir));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($cacheDir), $interceptors);

        return $invocation->proceed();
    }

    public function transChoice($id, $number, array $parameters = array(), $domain = NULL, $locale = NULL)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'transChoice');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($id, $number, $parameters, $domain, $locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($id, $number, $parameters, $domain, $locale), $interceptors);

        return $invocation->proceed();
    }

    public function trans($id, array $parameters = array(), $domain = NULL, $locale = NULL)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'trans');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($id, $parameters, $domain, $locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($id, $parameters, $domain, $locale), $interceptors);

        return $invocation->proceed();
    }

    public function setLocale($locale)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'setLocale');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($locale), $interceptors);

        return $invocation->proceed();
    }

    public function setFallbackLocales(array $locales)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'setFallbackLocales');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($locales));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($locales), $interceptors);

        return $invocation->proceed();
    }

    public function setFallbackLocale($locales)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'setFallbackLocale');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($locales));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($locales), $interceptors);

        return $invocation->proceed();
    }

    public function setConfigCacheFactory(\Symfony\Component\Config\ConfigCacheFactoryInterface $configCacheFactory)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'setConfigCacheFactory');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($configCacheFactory));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($configCacheFactory), $interceptors);

        return $invocation->proceed();
    }

    public function getMessages($locale = NULL)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'getMessages');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($locale), $interceptors);

        return $invocation->proceed();
    }

    public function getLocale()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'getLocale');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getFallbackLocales()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'getFallbackLocales');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function getCatalogue($locale = NULL)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'getCatalogue');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($locale), $interceptors);

        return $invocation->proceed();
    }

    public function dumpCatalogue($locale, \Symfony\Component\Config\ConfigCacheInterface $cache)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'dumpCatalogue');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($locale, $cache));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($locale, $cache), $interceptors);

        return $invocation->proceed();
    }

    public function addResource($format, $resource, $locale, $domain = NULL)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'addResource');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($format, $resource, $locale, $domain));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($format, $resource, $locale, $domain), $interceptors);

        return $invocation->proceed();
    }

    public function addLoader($format, \Symfony\Component\Translation\Loader\LoaderInterface $loader)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'addLoader');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($format, $loader));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($format, $loader), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }

    protected function loadCatalogue($locale)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'loadCatalogue');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($locale), $interceptors);

        return $invocation->proceed();
    }

    protected function initializeCatalogue($locale)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\FrameworkBundle\\Translation\\Translator', 'initializeCatalogue');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($locale), $interceptors);

        return $invocation->proceed();
    }

    protected function initialize()
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\FrameworkBundle\\Translation\\Translator', 'initialize');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    protected function getLoaders()
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'getLoaders');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    protected function computeFallbackLocales($locale)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'computeFallbackLocales');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($locale), $interceptors);

        return $invocation->proceed();
    }

    protected function assertValidLocale($locale)
    {
        $ref = new \ReflectionMethod('Symfony\\Component\\Translation\\Translator', 'assertValidLocale');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($locale));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($locale), $interceptors);

        return $invocation->proceed();
    }
}