<?php

namespace EnhancedProxyf57672ad_e9b402212669c8cdead5a9b3c93b89c3641c619c\__CG__\Symfony\Bundle\AsseticBundle\CacheWarmer;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class AssetManagerCacheWarmer extends \Symfony\Bundle\AsseticBundle\CacheWarmer\AssetManagerCacheWarmer
{
    private $__CGInterception__loader;

    public function warmUp($cacheDir)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\AsseticBundle\\CacheWarmer\\AssetManagerCacheWarmer', 'warmUp');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($cacheDir));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($cacheDir), $interceptors);

        return $invocation->proceed();
    }

    public function isOptional()
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\AsseticBundle\\CacheWarmer\\AssetManagerCacheWarmer', 'isOptional');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array());
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array(), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}