<?php

namespace EnhancedProxyf57672ad_1a6b61d387727149a6c4444e7ede60b2d9613c4b\__CG__\AppBundle\Aop;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class LoggingPointcut extends \AppBundle\Aop\LoggingPointcut
{
    private $__CGInterception__loader;

    public function matchesMethod(\ReflectionMethod $method)
    {
        $ref = new \ReflectionMethod('AppBundle\\Log\\LoggingPointcut', 'matchesMethod');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($method));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($method), $interceptors);

        return $invocation->proceed();
    }

    public function matchesClass(\ReflectionClass $class)
    {
        $ref = new \ReflectionMethod('AppBundle\\Log\\LoggingPointcut', 'matchesClass');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($class));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($class), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}