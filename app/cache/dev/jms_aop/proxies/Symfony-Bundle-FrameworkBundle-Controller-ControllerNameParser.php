<?php

namespace EnhancedProxyf57672ad_48d3566a9f4d645d5fb6446e6212a7c3a3c02746\__CG__\Symfony\Bundle\FrameworkBundle\Controller;

/**
 * CG library enhanced proxy class.
 *
 * This code was generated automatically by the CG library, manual changes to it
 * will be lost upon next generation.
 */
class ControllerNameParser extends \Symfony\Bundle\FrameworkBundle\Controller\ControllerNameParser
{
    private $__CGInterception__loader;

    public function parse($controller)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\FrameworkBundle\\Controller\\ControllerNameParser', 'parse');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($controller));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($controller), $interceptors);

        return $invocation->proceed();
    }

    public function build($controller)
    {
        $ref = new \ReflectionMethod('Symfony\\Bundle\\FrameworkBundle\\Controller\\ControllerNameParser', 'build');
        $interceptors = $this->__CGInterception__loader->loadInterceptors($ref, $this, array($controller));
        $invocation = new \CG\Proxy\MethodInvocation($ref, $this, array($controller), $interceptors);

        return $invocation->proceed();
    }

    public function __CGInterception__setLoader(\CG\Proxy\InterceptorLoaderInterface $loader)
    {
        $this->__CGInterception__loader = $loader;
    }
}