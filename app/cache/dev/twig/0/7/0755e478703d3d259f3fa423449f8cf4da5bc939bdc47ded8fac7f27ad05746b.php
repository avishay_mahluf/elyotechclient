<?php

/* validation/login.html.twig */
class __TwigTemplate_50225637ced74a92d6e150039010371dc6ba5b99daf97787a35d4cecc777cab4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "validation/login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_c983daba27a90ff821b5431708350b4e30ca9f9f8bfcef94ba8b74734947a9af = $this->env->getExtension("native_profiler");
        $__internal_c983daba27a90ff821b5431708350b4e30ca9f9f8bfcef94ba8b74734947a9af->enter($__internal_c983daba27a90ff821b5431708350b4e30ca9f9f8bfcef94ba8b74734947a9af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "validation/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_c983daba27a90ff821b5431708350b4e30ca9f9f8bfcef94ba8b74734947a9af->leave($__internal_c983daba27a90ff821b5431708350b4e30ca9f9f8bfcef94ba8b74734947a9af_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_b187b273c016d1b610ca9e0c4dc787530e453e5299462a51605bf7e968653104 = $this->env->getExtension("native_profiler");
        $__internal_b187b273c016d1b610ca9e0c4dc787530e453e5299462a51605bf7e968653104->enter($__internal_b187b273c016d1b610ca9e0c4dc787530e453e5299462a51605bf7e968653104_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-4 col-md-offset-4\">
            <div class=\"login-panel panel panel-default\">

                ";
        // line 9
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 10
            echo "                    <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "html", null, true);
            echo "</div>
                ";
        }
        // line 12
        echo "
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Please Sign In</h3>
                </div>
                <div class=\"panel-body\">
                    <form role=\"form\" action=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("verify");
        echo "\" method=\"post\">
                        <fieldset>
                            <div class=\"form-group\">
                                <input class=\"form-control\" placeholder=\"User Name\" id=\"username\" name=\"_username\" type=\"text\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" autofocus>
                            </div>
                            <div class=\"form-group\">
                                <input class=\"form-control\" placeholder=\"Password\" id=\"password\" name=\"_password\" type=\"password\" value=\"\">
                            </div>

                            <input type=\"hidden\" name=\"_target_path\" value=\"/\">

                            <button type=\"submit\" class=\"btn btn-lg btn-success btn-block\">Login</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_b187b273c016d1b610ca9e0c4dc787530e453e5299462a51605bf7e968653104->leave($__internal_b187b273c016d1b610ca9e0c4dc787530e453e5299462a51605bf7e968653104_prof);

    }

    // line 38
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_0f0cdc1ac7a2b4b6efda6aecc2810fb9ced627cd2809b8f8ea468c0ba910aa5f = $this->env->getExtension("native_profiler");
        $__internal_0f0cdc1ac7a2b4b6efda6aecc2810fb9ced627cd2809b8f8ea468c0ba910aa5f->enter($__internal_0f0cdc1ac7a2b4b6efda6aecc2810fb9ced627cd2809b8f8ea468c0ba910aa5f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 39
        echo "    ";
        echo twig_include($this->env, $context, "stylesheets.html.twig");
        echo "
";
        
        $__internal_0f0cdc1ac7a2b4b6efda6aecc2810fb9ced627cd2809b8f8ea468c0ba910aa5f->leave($__internal_0f0cdc1ac7a2b4b6efda6aecc2810fb9ced627cd2809b8f8ea468c0ba910aa5f_prof);

    }

    // line 43
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_85175b405013491f5c8f40974f4ca46c206e2e8e99acf0281adcd831fe7a7245 = $this->env->getExtension("native_profiler");
        $__internal_85175b405013491f5c8f40974f4ca46c206e2e8e99acf0281adcd831fe7a7245->enter($__internal_85175b405013491f5c8f40974f4ca46c206e2e8e99acf0281adcd831fe7a7245_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 44
        echo "    ";
        echo twig_include($this->env, $context, "javascripts.html.twig");
        echo "
";
        
        $__internal_85175b405013491f5c8f40974f4ca46c206e2e8e99acf0281adcd831fe7a7245->leave($__internal_85175b405013491f5c8f40974f4ca46c206e2e8e99acf0281adcd831fe7a7245_prof);

    }

    public function getTemplateName()
    {
        return "validation/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 44,  110 => 43,  100 => 39,  94 => 38,  70 => 20,  64 => 17,  57 => 12,  51 => 10,  49 => 9,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/* <div class="container">*/
/*     <div class="row">*/
/*         <div class="col-md-4 col-md-offset-4">*/
/*             <div class="login-panel panel panel-default">*/
/* */
/*                 {% if error %}*/
/*                     <div class="alert alert-danger">{{ error }}</div>*/
/*                 {% endif %}*/
/* */
/*                 <div class="panel-heading">*/
/*                     <h3 class="panel-title">Please Sign In</h3>*/
/*                 </div>*/
/*                 <div class="panel-body">*/
/*                     <form role="form" action="{{ path('verify') }}" method="post">*/
/*                         <fieldset>*/
/*                             <div class="form-group">*/
/*                                 <input class="form-control" placeholder="User Name" id="username" name="_username" type="text" value="{{ last_username }}" autofocus>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <input class="form-control" placeholder="Password" id="password" name="_password" type="password" value="">*/
/*                             </div>*/
/* */
/*                             <input type="hidden" name="_target_path" value="/">*/
/* */
/*                             <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>*/
/*                         </fieldset>*/
/*                     </form>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ include('stylesheets.html.twig') }}*/
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     {{ include('javascripts.html.twig') }}*/
/* {% endblock %}*/
