<?php

/* validation/login.html.twig */
class __TwigTemplate_c0f51b314076b3f9eb5db7d8e54e91d3b256d167c97aa16bee021d49e0fccf47 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "validation/login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_20b5317b05e4c40cecc23bd46e60147ff024c131083e7412224f34fedbe7eb5c = $this->env->getExtension("native_profiler");
        $__internal_20b5317b05e4c40cecc23bd46e60147ff024c131083e7412224f34fedbe7eb5c->enter($__internal_20b5317b05e4c40cecc23bd46e60147ff024c131083e7412224f34fedbe7eb5c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "validation/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_20b5317b05e4c40cecc23bd46e60147ff024c131083e7412224f34fedbe7eb5c->leave($__internal_20b5317b05e4c40cecc23bd46e60147ff024c131083e7412224f34fedbe7eb5c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_b7a74968d28de4f04e48072bd02c4cba9e1291dfb9e02a7c2eed593293d64a82 = $this->env->getExtension("native_profiler");
        $__internal_b7a74968d28de4f04e48072bd02c4cba9e1291dfb9e02a7c2eed593293d64a82->enter($__internal_b7a74968d28de4f04e48072bd02c4cba9e1291dfb9e02a7c2eed593293d64a82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-4 col-md-offset-4\">
            <div class=\"login-panel panel panel-default\">

                ";
        // line 9
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 10
            echo "                    <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, (isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "html", null, true);
            echo "</div>
                ";
        }
        // line 12
        echo "
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Please Sign In</h3>
                </div>
                <div class=\"panel-body\">
                    <form role=\"form\" action=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("verify");
        echo "\" method=\"post\">
                        <fieldset>
                            <div class=\"form-group\">
                                <input class=\"form-control\" placeholder=\"User Name\" id=\"username\" name=\"_username\" type=\"text\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" autofocus>
                            </div>
                            <div class=\"form-group\">
                                <input class=\"form-control\" placeholder=\"Password\" id=\"password\" name=\"_password\" type=\"password\" value=\"\">
                            </div>

                            <input type=\"hidden\" name=\"_target_path\" value=\"/\">

                            <button type=\"submit\" class=\"btn btn-lg btn-success btn-block\">Login</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_b7a74968d28de4f04e48072bd02c4cba9e1291dfb9e02a7c2eed593293d64a82->leave($__internal_b7a74968d28de4f04e48072bd02c4cba9e1291dfb9e02a7c2eed593293d64a82_prof);

    }

    // line 38
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_95964b8ed73bdf49f5f6a4b31d1828f8f771e16d806e19fedef35a0a3779b6d3 = $this->env->getExtension("native_profiler");
        $__internal_95964b8ed73bdf49f5f6a4b31d1828f8f771e16d806e19fedef35a0a3779b6d3->enter($__internal_95964b8ed73bdf49f5f6a4b31d1828f8f771e16d806e19fedef35a0a3779b6d3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 39
        echo "    ";
        echo twig_include($this->env, $context, "stylesheets.html.twig");
        echo "
";
        
        $__internal_95964b8ed73bdf49f5f6a4b31d1828f8f771e16d806e19fedef35a0a3779b6d3->leave($__internal_95964b8ed73bdf49f5f6a4b31d1828f8f771e16d806e19fedef35a0a3779b6d3_prof);

    }

    // line 43
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_36bd7a86a3cd9d7c20f5d94eaaa9b13aa7e740b75125f720f306d3518438add7 = $this->env->getExtension("native_profiler");
        $__internal_36bd7a86a3cd9d7c20f5d94eaaa9b13aa7e740b75125f720f306d3518438add7->enter($__internal_36bd7a86a3cd9d7c20f5d94eaaa9b13aa7e740b75125f720f306d3518438add7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 44
        echo "    ";
        echo twig_include($this->env, $context, "javascripts.html.twig");
        echo "
";
        
        $__internal_36bd7a86a3cd9d7c20f5d94eaaa9b13aa7e740b75125f720f306d3518438add7->leave($__internal_36bd7a86a3cd9d7c20f5d94eaaa9b13aa7e740b75125f720f306d3518438add7_prof);

    }

    public function getTemplateName()
    {
        return "validation/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 44,  110 => 43,  100 => 39,  94 => 38,  70 => 20,  64 => 17,  57 => 12,  51 => 10,  49 => 9,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/* <div class="container">*/
/*     <div class="row">*/
/*         <div class="col-md-4 col-md-offset-4">*/
/*             <div class="login-panel panel panel-default">*/
/* */
/*                 {% if error %}*/
/*                     <div class="alert alert-danger">{{ error }}</div>*/
/*                 {% endif %}*/
/* */
/*                 <div class="panel-heading">*/
/*                     <h3 class="panel-title">Please Sign In</h3>*/
/*                 </div>*/
/*                 <div class="panel-body">*/
/*                     <form role="form" action="{{ path('verify') }}" method="post">*/
/*                         <fieldset>*/
/*                             <div class="form-group">*/
/*                                 <input class="form-control" placeholder="User Name" id="username" name="_username" type="text" value="{{ last_username }}" autofocus>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <input class="form-control" placeholder="Password" id="password" name="_password" type="password" value="">*/
/*                             </div>*/
/* */
/*                             <input type="hidden" name="_target_path" value="/">*/
/* */
/*                             <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>*/
/*                         </fieldset>*/
/*                     </form>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ include('stylesheets.html.twig') }}*/
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     {{ include('javascripts.html.twig') }}*/
/* {% endblock %}*/
