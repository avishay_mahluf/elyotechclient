<?php

/* log/audit.html.twig */
class __TwigTemplate_71319a733e59ef879a4c482b8b01785679ed99a94f80b4cfa96621af138b4ce7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("table_responsive.html.twig", "log/audit.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'pageheader' => array($this, 'block_pageheader'),
            'thead' => array($this, 'block_thead'),
            'tbody' => array($this, 'block_tbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "table_responsive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_1a743c05c88b09a1dd7493283fc4808df14d8852ee5d6216279da12a15a15eff = $this->env->getExtension("native_profiler");
        $__internal_1a743c05c88b09a1dd7493283fc4808df14d8852ee5d6216279da12a15a15eff->enter($__internal_1a743c05c88b09a1dd7493283fc4808df14d8852ee5d6216279da12a15a15eff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "log/audit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_1a743c05c88b09a1dd7493283fc4808df14d8852ee5d6216279da12a15a15eff->leave($__internal_1a743c05c88b09a1dd7493283fc4808df14d8852ee5d6216279da12a15a15eff_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_132d0ced394812c37de73e91986cdc530eeedfebd33801943c578f4b1e390cbb = $this->env->getExtension("native_profiler");
        $__internal_132d0ced394812c37de73e91986cdc530eeedfebd33801943c578f4b1e390cbb->enter($__internal_132d0ced394812c37de73e91986cdc530eeedfebd33801943c578f4b1e390cbb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Audit Log";
        
        $__internal_132d0ced394812c37de73e91986cdc530eeedfebd33801943c578f4b1e390cbb->leave($__internal_132d0ced394812c37de73e91986cdc530eeedfebd33801943c578f4b1e390cbb_prof);

    }

    // line 4
    public function block_pageheader($context, array $blocks = array())
    {
        $__internal_4f2091355d0e7b0cded7e89ed1717b0f5483293ddeccdc864b4db3082f043e26 = $this->env->getExtension("native_profiler");
        $__internal_4f2091355d0e7b0cded7e89ed1717b0f5483293ddeccdc864b4db3082f043e26->enter($__internal_4f2091355d0e7b0cded7e89ed1717b0f5483293ddeccdc864b4db3082f043e26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pageheader"));

        echo "Audit Log";
        
        $__internal_4f2091355d0e7b0cded7e89ed1717b0f5483293ddeccdc864b4db3082f043e26->leave($__internal_4f2091355d0e7b0cded7e89ed1717b0f5483293ddeccdc864b4db3082f043e26_prof);

    }

    // line 6
    public function block_thead($context, array $blocks = array())
    {
        $__internal_62dd9d328a629ace7f90adbc2f116f076bb156b8b91e6e244fc575edb9a6700a = $this->env->getExtension("native_profiler");
        $__internal_62dd9d328a629ace7f90adbc2f116f076bb156b8b91e6e244fc575edb9a6700a->enter($__internal_62dd9d328a629ace7f90adbc2f116f076bb156b8b91e6e244fc575edb9a6700a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "thead"));

        // line 7
        echo "    <tr>
        <th>Date</th>
        <th>Field Name</th>
        <th>Changed From</th>
        <th>Changed To</th>
        <th>Environment ID</th>
        <th>Rollback</th>
    </tr>
";
        
        $__internal_62dd9d328a629ace7f90adbc2f116f076bb156b8b91e6e244fc575edb9a6700a->leave($__internal_62dd9d328a629ace7f90adbc2f116f076bb156b8b91e6e244fc575edb9a6700a_prof);

    }

    // line 17
    public function block_tbody($context, array $blocks = array())
    {
        $__internal_cb74eb4a1b2530314980ef7a956295fba1d04bf56b459dcb1925b3aa3a1c7d13 = $this->env->getExtension("native_profiler");
        $__internal_cb74eb4a1b2530314980ef7a956295fba1d04bf56b459dcb1925b3aa3a1c7d13->enter($__internal_cb74eb4a1b2530314980ef7a956295fba1d04bf56b459dcb1925b3aa3a1c7d13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tbody"));

        // line 18
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entries"]) ? $context["entries"] : $this->getContext($context, "entries")));
        foreach ($context['_seq'] as $context["_key"] => $context["entry"]) {
            // line 19
            echo "        <tr>
            <td>";
            // line 20
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "getCreated", array(), "method"), "Y-m-d h:m"), "html", null, true);
            echo "</td>
            <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "getFieldName", array(), "method"), "html", null, true);
            echo "</td>
            <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "getChangedFrom", array(), "method"), "html", null, true);
            echo "</td>
            <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "getChangedTo", array(), "method"), "html", null, true);
            echo "</td>
            <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "getEnvironmentId", array(), "method"), "html", null, true);
            echo "</td>
            <td>
                ";
            // line 26
            if (($this->getAttribute($context["entry"], "getRollBacked", array(), "method") == false)) {
                // line 27
                echo "                    <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("environment_revert", array("audit_id" => $this->getAttribute($context["entry"], "getId", array(), "method"))), "html", null, true);
                echo "\">
                        <i class=\"fa fa-undo\"></i>
                    </a>
                ";
            }
            // line 31
            echo "            </td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_cb74eb4a1b2530314980ef7a956295fba1d04bf56b459dcb1925b3aa3a1c7d13->leave($__internal_cb74eb4a1b2530314980ef7a956295fba1d04bf56b459dcb1925b3aa3a1c7d13_prof);

    }

    public function getTemplateName()
    {
        return "log/audit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 31,  119 => 27,  117 => 26,  112 => 24,  108 => 23,  104 => 22,  100 => 21,  96 => 20,  93 => 19,  88 => 18,  82 => 17,  67 => 7,  61 => 6,  49 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'table_responsive.html.twig' %}*/
/* */
/* {% block title %}Audit Log{% endblock %}*/
/* {% block pageheader %}Audit Log{% endblock %}*/
/* */
/* {% block thead %}*/
/*     <tr>*/
/*         <th>Date</th>*/
/*         <th>Field Name</th>*/
/*         <th>Changed From</th>*/
/*         <th>Changed To</th>*/
/*         <th>Environment ID</th>*/
/*         <th>Rollback</th>*/
/*     </tr>*/
/* {% endblock %}*/
/* */
/* {% block tbody %}*/
/*     {% for entry in entries %}*/
/*         <tr>*/
/*             <td>{{ entry.getCreated()|date('Y-m-d h:m') }}</td>*/
/*             <td>{{ entry.getFieldName() }}</td>*/
/*             <td>{{ entry.getChangedFrom() }}</td>*/
/*             <td>{{ entry.getChangedTo() }}</td>*/
/*             <td>{{ entry.getEnvironmentId() }}</td>*/
/*             <td>*/
/*                 {% if entry.getRollBacked() == false %}*/
/*                     <a href="{{ path('environment_revert', {'audit_id': entry.getId()}) }}">*/
/*                         <i class="fa fa-undo"></i>*/
/*                     </a>*/
/*                 {% endif %}*/
/*             </td>*/
/*         </tr>*/
/*     {% endfor %}*/
/* {% endblock %}*/
