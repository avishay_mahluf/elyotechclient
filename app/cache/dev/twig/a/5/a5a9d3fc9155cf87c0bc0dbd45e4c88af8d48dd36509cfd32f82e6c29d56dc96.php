<?php

/* environment/environment.html.twig */
class __TwigTemplate_af0769822ab0f1ac998594b220c1827754d64f084f231620345125b22880e20e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("table_responsive.html.twig", "environment/environment.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'pageheader' => array($this, 'block_pageheader'),
            'thead' => array($this, 'block_thead'),
            'tbody' => array($this, 'block_tbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "table_responsive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_446850b30a1b30cc60a125f955943aabfecb2372b46d7ecb870db671e444c386 = $this->env->getExtension("native_profiler");
        $__internal_446850b30a1b30cc60a125f955943aabfecb2372b46d7ecb870db671e444c386->enter($__internal_446850b30a1b30cc60a125f955943aabfecb2372b46d7ecb870db671e444c386_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "environment/environment.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_446850b30a1b30cc60a125f955943aabfecb2372b46d7ecb870db671e444c386->leave($__internal_446850b30a1b30cc60a125f955943aabfecb2372b46d7ecb870db671e444c386_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_abb054901657f24444049c4633f62a5ca75a4ad23c574c0a9bd9b56c1b0ee646 = $this->env->getExtension("native_profiler");
        $__internal_abb054901657f24444049c4633f62a5ca75a4ad23c574c0a9bd9b56c1b0ee646->enter($__internal_abb054901657f24444049c4633f62a5ca75a4ad23c574c0a9bd9b56c1b0ee646_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Environment";
        
        $__internal_abb054901657f24444049c4633f62a5ca75a4ad23c574c0a9bd9b56c1b0ee646->leave($__internal_abb054901657f24444049c4633f62a5ca75a4ad23c574c0a9bd9b56c1b0ee646_prof);

    }

    // line 4
    public function block_pageheader($context, array $blocks = array())
    {
        $__internal_2b0a310c57aceb6ba584e1cc23bab8d62f2befa2eea9090f4d1f22ac3a8dc3f2 = $this->env->getExtension("native_profiler");
        $__internal_2b0a310c57aceb6ba584e1cc23bab8d62f2befa2eea9090f4d1f22ac3a8dc3f2->enter($__internal_2b0a310c57aceb6ba584e1cc23bab8d62f2befa2eea9090f4d1f22ac3a8dc3f2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pageheader"));

        echo "Environment";
        
        $__internal_2b0a310c57aceb6ba584e1cc23bab8d62f2befa2eea9090f4d1f22ac3a8dc3f2->leave($__internal_2b0a310c57aceb6ba584e1cc23bab8d62f2befa2eea9090f4d1f22ac3a8dc3f2_prof);

    }

    // line 6
    public function block_thead($context, array $blocks = array())
    {
        $__internal_1bbbaf5b29d12d265a674c91d87b9d03ba962967aa21286fdd5fa7e6d6ea1dc1 = $this->env->getExtension("native_profiler");
        $__internal_1bbbaf5b29d12d265a674c91d87b9d03ba962967aa21286fdd5fa7e6d6ea1dc1->enter($__internal_1bbbaf5b29d12d265a674c91d87b9d03ba962967aa21286fdd5fa7e6d6ea1dc1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "thead"));

        // line 7
        echo "    <tr>
    <th>Edit</th>
    ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["columns_names"]) ? $context["columns_names"] : $this->getContext($context, "columns_names")));
        foreach ($context['_seq'] as $context["_key"] => $context["name"]) {
            // line 10
            echo "        <th>";
            echo twig_escape_filter($this->env, $context["name"], "html", null, true);
            echo "</th>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['name'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "    </tr>
";
        
        $__internal_1bbbaf5b29d12d265a674c91d87b9d03ba962967aa21286fdd5fa7e6d6ea1dc1->leave($__internal_1bbbaf5b29d12d265a674c91d87b9d03ba962967aa21286fdd5fa7e6d6ea1dc1_prof);

    }

    // line 15
    public function block_tbody($context, array $blocks = array())
    {
        $__internal_642ca60d44e3a5d07ae3ca8ad31bde932c2524a48da58033b2105cc118a00f55 = $this->env->getExtension("native_profiler");
        $__internal_642ca60d44e3a5d07ae3ca8ad31bde932c2524a48da58033b2105cc118a00f55->enter($__internal_642ca60d44e3a5d07ae3ca8ad31bde932c2524a48da58033b2105cc118a00f55_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tbody"));

        // line 16
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["env_results"]) ? $context["env_results"] : $this->getContext($context, "env_results")));
        foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
            // line 17
            echo "        <tr>
            <td>
                <a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("environment_show", array("environment_id" => $this->getAttribute($context["result"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-pencil-square-o\"></i></a>
            </td>
            ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->env->getExtension('to_array_extension')->to_array($context["result"]));
            foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                // line 22
                echo "                <td>";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "</td>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_642ca60d44e3a5d07ae3ca8ad31bde932c2524a48da58033b2105cc118a00f55->leave($__internal_642ca60d44e3a5d07ae3ca8ad31bde932c2524a48da58033b2105cc118a00f55_prof);

    }

    public function getTemplateName()
    {
        return "environment/environment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 24,  116 => 22,  112 => 21,  107 => 19,  103 => 17,  98 => 16,  92 => 15,  84 => 12,  75 => 10,  71 => 9,  67 => 7,  61 => 6,  49 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'table_responsive.html.twig' %}*/
/* */
/* {% block title %}Environment{% endblock %}*/
/* {% block pageheader %}Environment{% endblock %}*/
/* */
/* {% block thead %}*/
/*     <tr>*/
/*     <th>Edit</th>*/
/*     {% for name in columns_names %}*/
/*         <th>{{ name }}</th>*/
/*     {% endfor %}*/
/*     </tr>*/
/* {% endblock %}*/
/* */
/* {% block tbody %}*/
/*     {% for result in env_results %}*/
/*         <tr>*/
/*             <td>*/
/*                 <a href="{{ path('environment_show', {'environment_id': result.id}) }}"><i class="fa fa-pencil-square-o"></i></a>*/
/*             </td>*/
/*             {% for value in result|to_array %}*/
/*                 <td>{{ value }}</td>*/
/*             {% endfor %}*/
/*         </tr>*/
/*     {% endfor %}*/
/* {% endblock %}*/
