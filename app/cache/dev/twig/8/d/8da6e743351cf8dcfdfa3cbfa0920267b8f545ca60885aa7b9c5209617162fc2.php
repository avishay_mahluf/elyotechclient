<?php

/* base.html.twig */
class __TwigTemplate_6f3075e5707c367a2696dc36c1eb8e7875ffe6cb476094dec141a1ffc1a71f4f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fe5f2947d187eac4beb36d3c2c921ce037e187f5a72b86aded9e005bb7f33d96 = $this->env->getExtension("native_profiler");
        $__internal_fe5f2947d187eac4beb36d3c2c921ce037e187f5a72b86aded9e005bb7f33d96->enter($__internal_fe5f2947d187eac4beb36d3c2c921ce037e187f5a72b86aded9e005bb7f33d96_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>ElyoTech Test | ";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 12
        $this->displayBlock('body', $context, $blocks);
        // line 13
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 14
        echo "    </body>
</html>
";
        
        $__internal_fe5f2947d187eac4beb36d3c2c921ce037e187f5a72b86aded9e005bb7f33d96->leave($__internal_fe5f2947d187eac4beb36d3c2c921ce037e187f5a72b86aded9e005bb7f33d96_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_580b6cdc25bb5fccc9aec0e31af7ae231b80eef7f75d97dee1f4802e0f9bcf79 = $this->env->getExtension("native_profiler");
        $__internal_580b6cdc25bb5fccc9aec0e31af7ae231b80eef7f75d97dee1f4802e0f9bcf79->enter($__internal_580b6cdc25bb5fccc9aec0e31af7ae231b80eef7f75d97dee1f4802e0f9bcf79_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_580b6cdc25bb5fccc9aec0e31af7ae231b80eef7f75d97dee1f4802e0f9bcf79->leave($__internal_580b6cdc25bb5fccc9aec0e31af7ae231b80eef7f75d97dee1f4802e0f9bcf79_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_58560981458ce423fd8b9a9812a006b2bf05e123336485074f9d47190b40717d = $this->env->getExtension("native_profiler");
        $__internal_58560981458ce423fd8b9a9812a006b2bf05e123336485074f9d47190b40717d->enter($__internal_58560981458ce423fd8b9a9812a006b2bf05e123336485074f9d47190b40717d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_58560981458ce423fd8b9a9812a006b2bf05e123336485074f9d47190b40717d->leave($__internal_58560981458ce423fd8b9a9812a006b2bf05e123336485074f9d47190b40717d_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_877a3d20438e8b3c89810a34d7cdf55a9e560ab6b3b33193a5cc86a6f25b3623 = $this->env->getExtension("native_profiler");
        $__internal_877a3d20438e8b3c89810a34d7cdf55a9e560ab6b3b33193a5cc86a6f25b3623->enter($__internal_877a3d20438e8b3c89810a34d7cdf55a9e560ab6b3b33193a5cc86a6f25b3623_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_877a3d20438e8b3c89810a34d7cdf55a9e560ab6b3b33193a5cc86a6f25b3623->leave($__internal_877a3d20438e8b3c89810a34d7cdf55a9e560ab6b3b33193a5cc86a6f25b3623_prof);

    }

    // line 13
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_d282abb7bbb01f273bea03baced17bfcbc8b242649c91743af505ecd7c54f6b1 = $this->env->getExtension("native_profiler");
        $__internal_d282abb7bbb01f273bea03baced17bfcbc8b242649c91743af505ecd7c54f6b1->enter($__internal_d282abb7bbb01f273bea03baced17bfcbc8b242649c91743af505ecd7c54f6b1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_d282abb7bbb01f273bea03baced17bfcbc8b242649c91743af505ecd7c54f6b1->leave($__internal_d282abb7bbb01f273bea03baced17bfcbc8b242649c91743af505ecd7c54f6b1_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  83 => 12,  72 => 8,  61 => 7,  52 => 14,  49 => 13,  47 => 12,  40 => 9,  38 => 8,  34 => 7,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="utf-8">*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*         <title>ElyoTech Test | {% block title %}{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
