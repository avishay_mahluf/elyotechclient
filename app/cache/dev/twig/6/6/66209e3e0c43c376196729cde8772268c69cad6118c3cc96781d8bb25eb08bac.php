<?php

/* base.html.twig */
class __TwigTemplate_77f1b925c7f053140e7cb76d6b741b9e3aa32709d434b36a5f132c1abbee7207 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_24a8d063cf8b196ff6e2810fd31ff7339c416ce4d94e7a87af0e21a0ba762d8b = $this->env->getExtension("native_profiler");
        $__internal_24a8d063cf8b196ff6e2810fd31ff7339c416ce4d94e7a87af0e21a0ba762d8b->enter($__internal_24a8d063cf8b196ff6e2810fd31ff7339c416ce4d94e7a87af0e21a0ba762d8b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"utf-8\">
        <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">
        <meta name=\"viewport\" content=\"width=device-width, initial-scale=1\">
        <title>ElyoTech Test | ";
        // line 7
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 8
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 9
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
        ";
        // line 12
        $this->displayBlock('body', $context, $blocks);
        // line 13
        echo "        ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 14
        echo "    </body>
</html>
";
        
        $__internal_24a8d063cf8b196ff6e2810fd31ff7339c416ce4d94e7a87af0e21a0ba762d8b->leave($__internal_24a8d063cf8b196ff6e2810fd31ff7339c416ce4d94e7a87af0e21a0ba762d8b_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_18874678ae7e2fbaa097baa1944d048a99f086bf35f8223fb61ad5fa150b8b81 = $this->env->getExtension("native_profiler");
        $__internal_18874678ae7e2fbaa097baa1944d048a99f086bf35f8223fb61ad5fa150b8b81->enter($__internal_18874678ae7e2fbaa097baa1944d048a99f086bf35f8223fb61ad5fa150b8b81_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        
        $__internal_18874678ae7e2fbaa097baa1944d048a99f086bf35f8223fb61ad5fa150b8b81->leave($__internal_18874678ae7e2fbaa097baa1944d048a99f086bf35f8223fb61ad5fa150b8b81_prof);

    }

    // line 8
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_3709cff78bd76c37b721676f9e1e80c521cc10fe23869c95d36bde6183bbb122 = $this->env->getExtension("native_profiler");
        $__internal_3709cff78bd76c37b721676f9e1e80c521cc10fe23869c95d36bde6183bbb122->enter($__internal_3709cff78bd76c37b721676f9e1e80c521cc10fe23869c95d36bde6183bbb122_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        
        $__internal_3709cff78bd76c37b721676f9e1e80c521cc10fe23869c95d36bde6183bbb122->leave($__internal_3709cff78bd76c37b721676f9e1e80c521cc10fe23869c95d36bde6183bbb122_prof);

    }

    // line 12
    public function block_body($context, array $blocks = array())
    {
        $__internal_ef4359d7f17f5b3785cbe38177a77e05b8153342f049aa46e234674126c6179c = $this->env->getExtension("native_profiler");
        $__internal_ef4359d7f17f5b3785cbe38177a77e05b8153342f049aa46e234674126c6179c->enter($__internal_ef4359d7f17f5b3785cbe38177a77e05b8153342f049aa46e234674126c6179c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_ef4359d7f17f5b3785cbe38177a77e05b8153342f049aa46e234674126c6179c->leave($__internal_ef4359d7f17f5b3785cbe38177a77e05b8153342f049aa46e234674126c6179c_prof);

    }

    // line 13
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_608e58d3bdf6e90c491020ddc8113ccfaef2c2985d070d266c0a362512888ee1 = $this->env->getExtension("native_profiler");
        $__internal_608e58d3bdf6e90c491020ddc8113ccfaef2c2985d070d266c0a362512888ee1->enter($__internal_608e58d3bdf6e90c491020ddc8113ccfaef2c2985d070d266c0a362512888ee1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        
        $__internal_608e58d3bdf6e90c491020ddc8113ccfaef2c2985d070d266c0a362512888ee1->leave($__internal_608e58d3bdf6e90c491020ddc8113ccfaef2c2985d070d266c0a362512888ee1_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  94 => 13,  83 => 12,  72 => 8,  61 => 7,  52 => 14,  49 => 13,  47 => 12,  40 => 9,  38 => 8,  34 => 7,  26 => 1,);
    }
}
/* <!DOCTYPE html>*/
/* <html>*/
/*     <head>*/
/*         <meta charset="utf-8">*/
/*         <meta http-equiv="X-UA-Compatible" content="IE=edge">*/
/*         <meta name="viewport" content="width=device-width, initial-scale=1">*/
/*         <title>ElyoTech Test | {% block title %}{% endblock %}</title>*/
/*         {% block stylesheets %}{% endblock %}*/
/*         <link rel="icon" type="image/x-icon" href="{{ asset('favicon.ico') }}" />*/
/*     </head>*/
/*     <body>*/
/*         {% block body %}{% endblock %}*/
/*         {% block javascripts %}{% endblock %}*/
/*     </body>*/
/* </html>*/
/* */
