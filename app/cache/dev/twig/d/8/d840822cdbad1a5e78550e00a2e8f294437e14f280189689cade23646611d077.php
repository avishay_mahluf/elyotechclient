<?php

/* blank.html.twig */
class __TwigTemplate_84cd38688d8a3f088e96784819a2e961b5282be9ab4c68e22259bbfdcdd80ef4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "blank.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'nav' => array($this, 'block_nav'),
            'page_content' => array($this, 'block_page_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0d9ce2e308bebc89e6441435845e12cd005680a0a0581ad6ebc20e4480d24743 = $this->env->getExtension("native_profiler");
        $__internal_0d9ce2e308bebc89e6441435845e12cd005680a0a0581ad6ebc20e4480d24743->enter($__internal_0d9ce2e308bebc89e6441435845e12cd005680a0a0581ad6ebc20e4480d24743_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "blank.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0d9ce2e308bebc89e6441435845e12cd005680a0a0581ad6ebc20e4480d24743->leave($__internal_0d9ce2e308bebc89e6441435845e12cd005680a0a0581ad6ebc20e4480d24743_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_e5ad7df860e288730dc44f5aa8cd807e13f7979f6608480ce403a9d81796f529 = $this->env->getExtension("native_profiler");
        $__internal_e5ad7df860e288730dc44f5aa8cd807e13f7979f6608480ce403a9d81796f529->enter($__internal_e5ad7df860e288730dc44f5aa8cd807e13f7979f6608480ce403a9d81796f529_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div id=\"wrapper\">

        ";
        // line 6
        $this->displayBlock('nav', $context, $blocks);
        // line 9
        echo "
        <!-- Page Content -->
        <div id=\"page-wrapper\">
            <div class=\"container-fluid\">
                ";
        // line 13
        $this->displayBlock('page_content', $context, $blocks);
        // line 14
        echo "            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    ";
        
        $__internal_e5ad7df860e288730dc44f5aa8cd807e13f7979f6608480ce403a9d81796f529->leave($__internal_e5ad7df860e288730dc44f5aa8cd807e13f7979f6608480ce403a9d81796f529_prof);

    }

    // line 6
    public function block_nav($context, array $blocks = array())
    {
        $__internal_7c467d0a2e717b0c56b6aecc81ccd4a7dfa5d6d63529b3d61935f43fea633450 = $this->env->getExtension("native_profiler");
        $__internal_7c467d0a2e717b0c56b6aecc81ccd4a7dfa5d6d63529b3d61935f43fea633450->enter($__internal_7c467d0a2e717b0c56b6aecc81ccd4a7dfa5d6d63529b3d61935f43fea633450_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "nav"));

        // line 7
        echo "            ";
        echo twig_include($this->env, $context, "navigation.html.twig");
        echo "
        ";
        
        $__internal_7c467d0a2e717b0c56b6aecc81ccd4a7dfa5d6d63529b3d61935f43fea633450->leave($__internal_7c467d0a2e717b0c56b6aecc81ccd4a7dfa5d6d63529b3d61935f43fea633450_prof);

    }

    // line 13
    public function block_page_content($context, array $blocks = array())
    {
        $__internal_0e9450fd84e24bb6974ab0b65231396825d0dffcd8ca1800912a34afe7b9ce21 = $this->env->getExtension("native_profiler");
        $__internal_0e9450fd84e24bb6974ab0b65231396825d0dffcd8ca1800912a34afe7b9ce21->enter($__internal_0e9450fd84e24bb6974ab0b65231396825d0dffcd8ca1800912a34afe7b9ce21_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_content"));

        
        $__internal_0e9450fd84e24bb6974ab0b65231396825d0dffcd8ca1800912a34afe7b9ce21->leave($__internal_0e9450fd84e24bb6974ab0b65231396825d0dffcd8ca1800912a34afe7b9ce21_prof);

    }

    // line 23
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_8884c31d213119a01793fc830efc517626994a5e788a02a00b83eff837d630d1 = $this->env->getExtension("native_profiler");
        $__internal_8884c31d213119a01793fc830efc517626994a5e788a02a00b83eff837d630d1->enter($__internal_8884c31d213119a01793fc830efc517626994a5e788a02a00b83eff837d630d1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 24
        echo "    ";
        echo twig_include($this->env, $context, "stylesheets.html.twig");
        echo "
";
        
        $__internal_8884c31d213119a01793fc830efc517626994a5e788a02a00b83eff837d630d1->leave($__internal_8884c31d213119a01793fc830efc517626994a5e788a02a00b83eff837d630d1_prof);

    }

    // line 28
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_8eeb4ca0cc43806ce5757a18a3f0d90a5ea1ef220a527720ea3c1f6ca31636c8 = $this->env->getExtension("native_profiler");
        $__internal_8eeb4ca0cc43806ce5757a18a3f0d90a5ea1ef220a527720ea3c1f6ca31636c8->enter($__internal_8eeb4ca0cc43806ce5757a18a3f0d90a5ea1ef220a527720ea3c1f6ca31636c8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 29
        echo "    ";
        echo twig_include($this->env, $context, "javascripts.html.twig");
        echo "
";
        
        $__internal_8eeb4ca0cc43806ce5757a18a3f0d90a5ea1ef220a527720ea3c1f6ca31636c8->leave($__internal_8eeb4ca0cc43806ce5757a18a3f0d90a5ea1ef220a527720ea3c1f6ca31636c8_prof);

    }

    public function getTemplateName()
    {
        return "blank.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 29,  114 => 28,  104 => 24,  98 => 23,  87 => 13,  77 => 7,  71 => 6,  58 => 14,  56 => 13,  50 => 9,  48 => 6,  44 => 4,  38 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <div id="wrapper">*/
/* */
/*         {% block nav %}*/
/*             {{ include('navigation.html.twig') }}*/
/*         {% endblock %}*/
/* */
/*         <!-- Page Content -->*/
/*         <div id="page-wrapper">*/
/*             <div class="container-fluid">*/
/*                 {% block page_content %}{% endblock %}*/
/*             </div>*/
/*             <!-- /.container-fluid -->*/
/*         </div>*/
/*         <!-- /#page-wrapper -->*/
/* */
/*     </div>*/
/*     {# wrapper #}*/
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ include('stylesheets.html.twig') }}*/
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     {{ include('javascripts.html.twig') }}*/
/* {% endblock %}*/
