<?php

/* javascripts.html.twig */
class __TwigTemplate_af746a8084e6acdb26b7d71375e344ca46203e9406e27a1b2a7dee02e78e2012 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_547814d593ff8e4758c692bcea8ac01179c4b5610af840ade3e2db93e90a295e = $this->env->getExtension("native_profiler");
        $__internal_547814d593ff8e4758c692bcea8ac01179c4b5610af840ade3e2db93e90a295e->enter($__internal_547814d593ff8e4758c692bcea8ac01179c4b5610af840ade3e2db93e90a295e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "javascripts.html.twig"));

        // line 1
        echo "<!-- jQuery -->
<script src=\"";
        // line 2
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bower_components/jquery/dist/jquery.min.js"), "html", null, true);
        echo "\"></script>

<!-- Bootstrap Core JavaScript -->
<script src=\"";
        // line 5
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bower_components/bootstrap/dist/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>

<!-- Metis Menu Plugin JavaScript -->
<script src=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bower_components/metisMenu/dist/metisMenu.min.js"), "html", null, true);
        echo "\"></script>

<!-- Morris Charts JavaScript -->
<script src=\"";
        // line 11
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bower_components/raphael/raphael-min.js"), "html", null, true);
        echo "\"></script>
<script src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("bower_components/morrisjs/morris.min.js"), "html", null, true);
        echo "\"></script>

<!-- Custom Theme JavaScript -->
<script src=\"";
        // line 15
        echo twig_escape_filter($this->env, $this->env->getExtension('asset')->getAssetUrl("dist/js/sb-admin-2.js"), "html", null, true);
        echo "\"></script>";
        
        $__internal_547814d593ff8e4758c692bcea8ac01179c4b5610af840ade3e2db93e90a295e->leave($__internal_547814d593ff8e4758c692bcea8ac01179c4b5610af840ade3e2db93e90a295e_prof);

    }

    public function getTemplateName()
    {
        return "javascripts.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  53 => 15,  47 => 12,  43 => 11,  37 => 8,  31 => 5,  25 => 2,  22 => 1,);
    }
}
/* <!-- jQuery -->*/
/* <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>*/
/* */
/* <!-- Bootstrap Core JavaScript -->*/
/* <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>*/
/* */
/* <!-- Metis Menu Plugin JavaScript -->*/
/* <script src="{{ asset('bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>*/
/* */
/* <!-- Morris Charts JavaScript -->*/
/* <script src="{{ asset('bower_components/raphael/raphael-min.js') }}"></script>*/
/* <script src="{{ asset('bower_components/morrisjs/morris.min.js') }}"></script>*/
/* */
/* <!-- Custom Theme JavaScript -->*/
/* <script src="{{ asset('dist/js/sb-admin-2.js') }}"></script>*/
