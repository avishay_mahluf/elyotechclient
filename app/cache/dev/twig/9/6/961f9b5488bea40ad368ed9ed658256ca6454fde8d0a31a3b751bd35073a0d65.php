<?php

/* default/login.html.twig */
class __TwigTemplate_82416bd1a6f78a9eee825b3fed217163fa32dd3812e77eeaaf190cd911552f6d extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/login.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_367c525642054e220a4132ce8490a8dda01bfa6cb43d2a0d938a2af6dc0fa91e = $this->env->getExtension("native_profiler");
        $__internal_367c525642054e220a4132ce8490a8dda01bfa6cb43d2a0d938a2af6dc0fa91e->enter($__internal_367c525642054e220a4132ce8490a8dda01bfa6cb43d2a0d938a2af6dc0fa91e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/login.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_367c525642054e220a4132ce8490a8dda01bfa6cb43d2a0d938a2af6dc0fa91e->leave($__internal_367c525642054e220a4132ce8490a8dda01bfa6cb43d2a0d938a2af6dc0fa91e_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_aa4729b868a8025ad1aad965d592e5ad46cceef992af19d35b948629f9da4bbd = $this->env->getExtension("native_profiler");
        $__internal_aa4729b868a8025ad1aad965d592e5ad46cceef992af19d35b948629f9da4bbd->enter($__internal_aa4729b868a8025ad1aad965d592e5ad46cceef992af19d35b948629f9da4bbd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "<div class=\"container\">
    <div class=\"row\">
        <div class=\"col-md-4 col-md-offset-4\">
            <div class=\"login-panel panel panel-default\">

                ";
        // line 9
        if ((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error"))) {
            // line 10
            echo "                    <div class=\"alert alert-danger\">";
            echo twig_escape_filter($this->env, $this->env->getExtension('translator')->trans($this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageKey", array()), $this->getAttribute((isset($context["error"]) ? $context["error"] : $this->getContext($context, "error")), "messageData", array()), "security"), "html", null, true);
            echo "</div>
                ";
        }
        // line 12
        echo "
                <div class=\"panel-heading\">
                    <h3 class=\"panel-title\">Please Sign In</h3>
                </div>
                <div class=\"panel-body\">
                    <form role=\"form\" action=\"";
        // line 17
        echo $this->env->getExtension('routing')->getPath("verify");
        echo "\" method=\"post\">
                        <fieldset>
                            <div class=\"form-group\">
                                <input class=\"form-control\" placeholder=\"User Name\" id=\"username\" name=\"_username\" type=\"text\" value=\"";
        // line 20
        echo twig_escape_filter($this->env, (isset($context["last_username"]) ? $context["last_username"] : $this->getContext($context, "last_username")), "html", null, true);
        echo "\" autofocus>
                            </div>
                            <div class=\"form-group\">
                                <input class=\"form-control\" placeholder=\"Password\" id=\"password\" name=\"_password\" type=\"password\" value=\"\">
                            </div>

                            <input type=\"hidden\" name=\"_target_path\" value=\"/\">

                            <button type=\"submit\" class=\"btn btn-lg btn-success btn-block\">Login</button>
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
";
        
        $__internal_aa4729b868a8025ad1aad965d592e5ad46cceef992af19d35b948629f9da4bbd->leave($__internal_aa4729b868a8025ad1aad965d592e5ad46cceef992af19d35b948629f9da4bbd_prof);

    }

    // line 38
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_ecce8d424e839efcc08645e94bcb2f00ac9c2533c1168536304fe4d1d1ec5346 = $this->env->getExtension("native_profiler");
        $__internal_ecce8d424e839efcc08645e94bcb2f00ac9c2533c1168536304fe4d1d1ec5346->enter($__internal_ecce8d424e839efcc08645e94bcb2f00ac9c2533c1168536304fe4d1d1ec5346_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 39
        echo "    ";
        echo twig_include($this->env, $context, "stylesheets.html.twig");
        echo "
";
        
        $__internal_ecce8d424e839efcc08645e94bcb2f00ac9c2533c1168536304fe4d1d1ec5346->leave($__internal_ecce8d424e839efcc08645e94bcb2f00ac9c2533c1168536304fe4d1d1ec5346_prof);

    }

    // line 43
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_c0130e705ed46ced4a655dac69a3aa254fc90efcdaf5c3acf0d422ec70a28077 = $this->env->getExtension("native_profiler");
        $__internal_c0130e705ed46ced4a655dac69a3aa254fc90efcdaf5c3acf0d422ec70a28077->enter($__internal_c0130e705ed46ced4a655dac69a3aa254fc90efcdaf5c3acf0d422ec70a28077_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 44
        echo "    ";
        echo twig_include($this->env, $context, "javascripts.html.twig");
        echo "
";
        
        $__internal_c0130e705ed46ced4a655dac69a3aa254fc90efcdaf5c3acf0d422ec70a28077->leave($__internal_c0130e705ed46ced4a655dac69a3aa254fc90efcdaf5c3acf0d422ec70a28077_prof);

    }

    public function getTemplateName()
    {
        return "default/login.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  116 => 44,  110 => 43,  100 => 39,  94 => 38,  70 => 20,  64 => 17,  57 => 12,  51 => 10,  49 => 9,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/* <div class="container">*/
/*     <div class="row">*/
/*         <div class="col-md-4 col-md-offset-4">*/
/*             <div class="login-panel panel panel-default">*/
/* */
/*                 {% if error %}*/
/*                     <div class="alert alert-danger">{{ error.messageKey|trans(error.messageData, 'security') }}</div>*/
/*                 {% endif %}*/
/* */
/*                 <div class="panel-heading">*/
/*                     <h3 class="panel-title">Please Sign In</h3>*/
/*                 </div>*/
/*                 <div class="panel-body">*/
/*                     <form role="form" action="{{ path('verify') }}" method="post">*/
/*                         <fieldset>*/
/*                             <div class="form-group">*/
/*                                 <input class="form-control" placeholder="User Name" id="username" name="_username" type="text" value="{{ last_username }}" autofocus>*/
/*                             </div>*/
/*                             <div class="form-group">*/
/*                                 <input class="form-control" placeholder="Password" id="password" name="_password" type="password" value="">*/
/*                             </div>*/
/* */
/*                             <input type="hidden" name="_target_path" value="/">*/
/* */
/*                             <button type="submit" class="btn btn-lg btn-success btn-block">Login</button>*/
/*                         </fieldset>*/
/*                     </form>*/
/*                 </div>*/
/*             </div>*/
/*         </div>*/
/*     </div>*/
/* </div>*/
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ include('stylesheets.html.twig') }}*/
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     {{ include('javascripts.html.twig') }}*/
/* {% endblock %}*/
