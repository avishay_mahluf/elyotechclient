<?php

/* environment/environment_edit.html.twig */
class __TwigTemplate_ac6e2ce175be8e76c10e787873e4bcea93fce003a6a2115239d33109e7427b8a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("blank.html.twig", "environment/environment_edit.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'page_content' => array($this, 'block_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "blank.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_be5f3ba6ed88b5eb3a99e502a9b50489fa45a5b77a4550c0dfdb0ba4315b8522 = $this->env->getExtension("native_profiler");
        $__internal_be5f3ba6ed88b5eb3a99e502a9b50489fa45a5b77a4550c0dfdb0ba4315b8522->enter($__internal_be5f3ba6ed88b5eb3a99e502a9b50489fa45a5b77a4550c0dfdb0ba4315b8522_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "environment/environment_edit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_be5f3ba6ed88b5eb3a99e502a9b50489fa45a5b77a4550c0dfdb0ba4315b8522->leave($__internal_be5f3ba6ed88b5eb3a99e502a9b50489fa45a5b77a4550c0dfdb0ba4315b8522_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_55722a572c10c8c5f145d06ef8327d1c5d021cc8e43e5601a97404787641d7af = $this->env->getExtension("native_profiler");
        $__internal_55722a572c10c8c5f145d06ef8327d1c5d021cc8e43e5601a97404787641d7af->enter($__internal_55722a572c10c8c5f145d06ef8327d1c5d021cc8e43e5601a97404787641d7af_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Edit Environment";
        
        $__internal_55722a572c10c8c5f145d06ef8327d1c5d021cc8e43e5601a97404787641d7af->leave($__internal_55722a572c10c8c5f145d06ef8327d1c5d021cc8e43e5601a97404787641d7af_prof);

    }

    // line 4
    public function block_page_content($context, array $blocks = array())
    {
        $__internal_36c29787a4bdc0fade6ee0315a6dc201b74889a20ec721905192c1ddbf3152c0 = $this->env->getExtension("native_profiler");
        $__internal_36c29787a4bdc0fade6ee0315a6dc201b74889a20ec721905192c1ddbf3152c0->enter($__internal_36c29787a4bdc0fade6ee0315a6dc201b74889a20ec721905192c1ddbf3152c0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_content"));

        // line 5
        echo "    <div class=\"row\">
        <div class=\"col-lg-8\">
            <h1 class=\"page-header\">Edit Environment</h1>

            ";
        // line 9
        if ((isset($context["message"]) ? $context["message"] : $this->getContext($context, "message"))) {
            // line 10
            echo "                <div class=\"alert alert-info\">
                    ";
            // line 11
            echo twig_escape_filter($this->env, (isset($context["message"]) ? $context["message"] : $this->getContext($context, "message")), "html", null, true);
            echo "
                </div>
            ";
        }
        // line 14
        echo "
            <form class=\"form-horizontal\" role=\"form\" method=\"post\" action=\"";
        // line 15
        echo $this->env->getExtension('routing')->getPath("environment_edit");
        echo "\">

                ";
        // line 17
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($this->env->getExtension('to_array_extension')->to_array((isset($context["env_object"]) ? $context["env_object"] : $this->getContext($context, "env_object"))));
        foreach ($context['_seq'] as $context["key"] => $context["value"]) {
            // line 18
            echo "                    <div class=\"form-group\">
                        <label class=\"control-label col-sm-4\" for=\"email\">";
            // line 19
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo ":</label>
                        <div class=\"col-sm-8\">
                            <input type=\"text\" class=\"form-control\"
                                   name=\"";
            // line 22
            echo twig_escape_filter($this->env, $context["key"], "html", null, true);
            echo "\" value=\"";
            echo twig_escape_filter($this->env, $context["value"], "html", null, true);
            echo "\"
                                    ";
            // line 23
            if (($context["key"] == "id")) {
                echo " readonly ";
            }
            echo ">
                        </div>
                    </div>
                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 27
        echo "
                <div class=\"form-group\">
                    <div class=\"col-sm-offset-4 col-sm-10\">
                        <button type=\"submit\" class=\"btn btn-default\">Submit</button>
                    </div>
                </div>
            </form>

        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
";
        
        $__internal_36c29787a4bdc0fade6ee0315a6dc201b74889a20ec721905192c1ddbf3152c0->leave($__internal_36c29787a4bdc0fade6ee0315a6dc201b74889a20ec721905192c1ddbf3152c0_prof);

    }

    public function getTemplateName()
    {
        return "environment/environment_edit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  109 => 27,  97 => 23,  91 => 22,  85 => 19,  82 => 18,  78 => 17,  73 => 15,  70 => 14,  64 => 11,  61 => 10,  59 => 9,  53 => 5,  47 => 4,  35 => 3,  11 => 1,);
    }
}
/* {% extends 'blank.html.twig' %}*/
/* */
/* {% block title %}Edit Environment{% endblock %}*/
/* {% block page_content %}*/
/*     <div class="row">*/
/*         <div class="col-lg-8">*/
/*             <h1 class="page-header">Edit Environment</h1>*/
/* */
/*             {% if message %}*/
/*                 <div class="alert alert-info">*/
/*                     {{ message }}*/
/*                 </div>*/
/*             {% endif %}*/
/* */
/*             <form class="form-horizontal" role="form" method="post" action="{{ path('environment_edit') }}">*/
/* */
/*                 {% for key,value in env_object|to_array %}*/
/*                     <div class="form-group">*/
/*                         <label class="control-label col-sm-4" for="email">{{ key }}:</label>*/
/*                         <div class="col-sm-8">*/
/*                             <input type="text" class="form-control"*/
/*                                    name="{{ key }}" value="{{ value }}"*/
/*                                     {%  if key == 'id' %} readonly {% endif %}>*/
/*                         </div>*/
/*                     </div>*/
/*                 {% endfor %}*/
/* */
/*                 <div class="form-group">*/
/*                     <div class="col-sm-offset-4 col-sm-10">*/
/*                         <button type="submit" class="btn btn-default">Submit</button>*/
/*                     </div>*/
/*                 </div>*/
/*             </form>*/
/* */
/*         </div>*/
/*         <!-- /.col-lg-12 -->*/
/*     </div>*/
/*     <!-- /.row -->*/
/* {% endblock %}*/
