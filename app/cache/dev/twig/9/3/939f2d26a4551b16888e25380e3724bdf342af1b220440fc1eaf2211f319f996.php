<?php

/* log/audit.html.twig */
class __TwigTemplate_fb603050b8ac2069f41a91719a913e17c8e56ceffb11fb9f93ea56abd47a813a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("table_responsive.html.twig", "log/audit.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'pageheader' => array($this, 'block_pageheader'),
            'thead' => array($this, 'block_thead'),
            'tbody' => array($this, 'block_tbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "table_responsive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cefa6a0e5f747334385e5a313b2a73e0bfaeac71df299d8a3f1953ee4dd987f4 = $this->env->getExtension("native_profiler");
        $__internal_cefa6a0e5f747334385e5a313b2a73e0bfaeac71df299d8a3f1953ee4dd987f4->enter($__internal_cefa6a0e5f747334385e5a313b2a73e0bfaeac71df299d8a3f1953ee4dd987f4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "log/audit.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cefa6a0e5f747334385e5a313b2a73e0bfaeac71df299d8a3f1953ee4dd987f4->leave($__internal_cefa6a0e5f747334385e5a313b2a73e0bfaeac71df299d8a3f1953ee4dd987f4_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_6ee83d56aa6522716aae3ad1cce8502952d8a018312855144f89a05e8ed2a1c7 = $this->env->getExtension("native_profiler");
        $__internal_6ee83d56aa6522716aae3ad1cce8502952d8a018312855144f89a05e8ed2a1c7->enter($__internal_6ee83d56aa6522716aae3ad1cce8502952d8a018312855144f89a05e8ed2a1c7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Audit Log";
        
        $__internal_6ee83d56aa6522716aae3ad1cce8502952d8a018312855144f89a05e8ed2a1c7->leave($__internal_6ee83d56aa6522716aae3ad1cce8502952d8a018312855144f89a05e8ed2a1c7_prof);

    }

    // line 4
    public function block_pageheader($context, array $blocks = array())
    {
        $__internal_531e047c3da355dad3323ca21dafd69da8f85919af5662e856ffb4f11e3b72dc = $this->env->getExtension("native_profiler");
        $__internal_531e047c3da355dad3323ca21dafd69da8f85919af5662e856ffb4f11e3b72dc->enter($__internal_531e047c3da355dad3323ca21dafd69da8f85919af5662e856ffb4f11e3b72dc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pageheader"));

        echo "Audit Log";
        
        $__internal_531e047c3da355dad3323ca21dafd69da8f85919af5662e856ffb4f11e3b72dc->leave($__internal_531e047c3da355dad3323ca21dafd69da8f85919af5662e856ffb4f11e3b72dc_prof);

    }

    // line 6
    public function block_thead($context, array $blocks = array())
    {
        $__internal_6dda8b648d816e19030fd1e2e319ce71b34bdf744d3c6901f2274b7842582fc2 = $this->env->getExtension("native_profiler");
        $__internal_6dda8b648d816e19030fd1e2e319ce71b34bdf744d3c6901f2274b7842582fc2->enter($__internal_6dda8b648d816e19030fd1e2e319ce71b34bdf744d3c6901f2274b7842582fc2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "thead"));

        // line 7
        echo "    <tr>
        <th>Date</th>
        <th>Field Name</th>
        <th>Changed From</th>
        <th>Changed To</th>
        <th>Environment ID</th>
        <th>Rollback</th>
    </tr>
";
        
        $__internal_6dda8b648d816e19030fd1e2e319ce71b34bdf744d3c6901f2274b7842582fc2->leave($__internal_6dda8b648d816e19030fd1e2e319ce71b34bdf744d3c6901f2274b7842582fc2_prof);

    }

    // line 17
    public function block_tbody($context, array $blocks = array())
    {
        $__internal_c13687ace8ac466b2ce57bdd39464ca542951323ae2991754d911151fade7f93 = $this->env->getExtension("native_profiler");
        $__internal_c13687ace8ac466b2ce57bdd39464ca542951323ae2991754d911151fade7f93->enter($__internal_c13687ace8ac466b2ce57bdd39464ca542951323ae2991754d911151fade7f93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tbody"));

        // line 18
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["entries"]) ? $context["entries"] : $this->getContext($context, "entries")));
        foreach ($context['_seq'] as $context["_key"] => $context["entry"]) {
            // line 19
            echo "        <tr>
            <td>";
            // line 20
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute($context["entry"], "getCreated", array(), "method"), "Y-m-d h:m"), "html", null, true);
            echo "</td>
            <td>";
            // line 21
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "getFieldName", array(), "method"), "html", null, true);
            echo "</td>
            <td>";
            // line 22
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "getChangedFrom", array(), "method"), "html", null, true);
            echo "</td>
            <td>";
            // line 23
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "getChangedTo", array(), "method"), "html", null, true);
            echo "</td>
            <td>";
            // line 24
            echo twig_escape_filter($this->env, $this->getAttribute($context["entry"], "getEnvironmentId", array(), "method"), "html", null, true);
            echo "</td>
            <td>
                ";
            // line 26
            if (($this->getAttribute($context["entry"], "getRollBacked", array(), "method") == false)) {
                // line 27
                echo "                    <a href=\"";
                echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("environment_revert", array("audit_id" => $this->getAttribute($context["entry"], "getId", array(), "method"))), "html", null, true);
                echo "\">
                        <i class=\"fa fa-undo\"></i>
                    </a>
                ";
            }
            // line 31
            echo "            </td>
        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['entry'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_c13687ace8ac466b2ce57bdd39464ca542951323ae2991754d911151fade7f93->leave($__internal_c13687ace8ac466b2ce57bdd39464ca542951323ae2991754d911151fade7f93_prof);

    }

    public function getTemplateName()
    {
        return "log/audit.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  127 => 31,  119 => 27,  117 => 26,  112 => 24,  108 => 23,  104 => 22,  100 => 21,  96 => 20,  93 => 19,  88 => 18,  82 => 17,  67 => 7,  61 => 6,  49 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'table_responsive.html.twig' %}*/
/* */
/* {% block title %}Audit Log{% endblock %}*/
/* {% block pageheader %}Audit Log{% endblock %}*/
/* */
/* {% block thead %}*/
/*     <tr>*/
/*         <th>Date</th>*/
/*         <th>Field Name</th>*/
/*         <th>Changed From</th>*/
/*         <th>Changed To</th>*/
/*         <th>Environment ID</th>*/
/*         <th>Rollback</th>*/
/*     </tr>*/
/* {% endblock %}*/
/* */
/* {% block tbody %}*/
/*     {% for entry in entries %}*/
/*         <tr>*/
/*             <td>{{ entry.getCreated()|date('Y-m-d h:m') }}</td>*/
/*             <td>{{ entry.getFieldName() }}</td>*/
/*             <td>{{ entry.getChangedFrom() }}</td>*/
/*             <td>{{ entry.getChangedTo() }}</td>*/
/*             <td>{{ entry.getEnvironmentId() }}</td>*/
/*             <td>*/
/*                 {% if entry.getRollBacked() == false %}*/
/*                     <a href="{{ path('environment_revert', {'audit_id': entry.getId()}) }}">*/
/*                         <i class="fa fa-undo"></i>*/
/*                     </a>*/
/*                 {% endif %}*/
/*             </td>*/
/*         </tr>*/
/*     {% endfor %}*/
/* {% endblock %}*/
