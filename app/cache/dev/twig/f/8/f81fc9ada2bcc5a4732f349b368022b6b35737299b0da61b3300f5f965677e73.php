<?php

/* workers/worker.html.twig */
class __TwigTemplate_6e9da0b660a6cf2f8e726c187957c70ceee1b8c52619ca4a559e663d1d87409e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("table_responsive.html.twig", "workers/worker.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'pageheader' => array($this, 'block_pageheader'),
            'thead' => array($this, 'block_thead'),
            'tbody' => array($this, 'block_tbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "table_responsive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_bafea0bb375f8b509e967ab0127a8a6ae0144725caa388079176144769800419 = $this->env->getExtension("native_profiler");
        $__internal_bafea0bb375f8b509e967ab0127a8a6ae0144725caa388079176144769800419->enter($__internal_bafea0bb375f8b509e967ab0127a8a6ae0144725caa388079176144769800419_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "workers/worker.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_bafea0bb375f8b509e967ab0127a8a6ae0144725caa388079176144769800419->leave($__internal_bafea0bb375f8b509e967ab0127a8a6ae0144725caa388079176144769800419_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_c681545e566350d88b572cd92e8e1db81e6af3ea88663ef51341ff741157853c = $this->env->getExtension("native_profiler");
        $__internal_c681545e566350d88b572cd92e8e1db81e6af3ea88663ef51341ff741157853c->enter($__internal_c681545e566350d88b572cd92e8e1db81e6af3ea88663ef51341ff741157853c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Workers";
        
        $__internal_c681545e566350d88b572cd92e8e1db81e6af3ea88663ef51341ff741157853c->leave($__internal_c681545e566350d88b572cd92e8e1db81e6af3ea88663ef51341ff741157853c_prof);

    }

    // line 4
    public function block_pageheader($context, array $blocks = array())
    {
        $__internal_23530ed1a288b5a07c6ceeb0941da6538efb82bd144d86c5799cb7476d6c54bc = $this->env->getExtension("native_profiler");
        $__internal_23530ed1a288b5a07c6ceeb0941da6538efb82bd144d86c5799cb7476d6c54bc->enter($__internal_23530ed1a288b5a07c6ceeb0941da6538efb82bd144d86c5799cb7476d6c54bc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pageheader"));

        echo "Workers";
        
        $__internal_23530ed1a288b5a07c6ceeb0941da6538efb82bd144d86c5799cb7476d6c54bc->leave($__internal_23530ed1a288b5a07c6ceeb0941da6538efb82bd144d86c5799cb7476d6c54bc_prof);

    }

    // line 6
    public function block_thead($context, array $blocks = array())
    {
        $__internal_71ac4239b9e166769f74262023a9831a5c34626db1f7fb814d591b98233b3ebc = $this->env->getExtension("native_profiler");
        $__internal_71ac4239b9e166769f74262023a9831a5c34626db1f7fb814d591b98233b3ebc->enter($__internal_71ac4239b9e166769f74262023a9831a5c34626db1f7fb814d591b98233b3ebc_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "thead"));

        // line 7
        echo "    <tr>
        ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["columns_names"]) ? $context["columns_names"] : $this->getContext($context, "columns_names")));
        foreach ($context['_seq'] as $context["_key"] => $context["name"]) {
            // line 9
            echo "            <th>";
            echo twig_escape_filter($this->env, $context["name"], "html", null, true);
            echo "</th>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['name'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "    </tr>
";
        
        $__internal_71ac4239b9e166769f74262023a9831a5c34626db1f7fb814d591b98233b3ebc->leave($__internal_71ac4239b9e166769f74262023a9831a5c34626db1f7fb814d591b98233b3ebc_prof);

    }

    // line 14
    public function block_tbody($context, array $blocks = array())
    {
        $__internal_934be5a22a14ca44d760f3e8f5fced1bdc501a2d4a2204b54ba00e98a69bc474 = $this->env->getExtension("native_profiler");
        $__internal_934be5a22a14ca44d760f3e8f5fced1bdc501a2d4a2204b54ba00e98a69bc474->enter($__internal_934be5a22a14ca44d760f3e8f5fced1bdc501a2d4a2204b54ba00e98a69bc474_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tbody"));

        // line 15
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["env_results"]) ? $context["env_results"] : $this->getContext($context, "env_results")));
        foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
            // line 16
            echo "        <tr>
            ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->env->getExtension('to_array_extension')->to_array($context["result"]));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                // line 18
                echo "                ";
                if (($context["key"] == "environment")) {
                    // line 19
                    echo "                    <td>
                        <a href=\"";
                    // line 20
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("environment_show", array("environment_id" => $this->getAttribute($context["value"], "id", array()))), "html", null, true);
                    echo "\"><i class=\"fa fa-pencil-square-o\"></i></a>
                    </td>
                ";
                } else {
                    // line 23
                    echo "                    <td>";
                    echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                    echo "</td>
                ";
                }
                // line 25
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_934be5a22a14ca44d760f3e8f5fced1bdc501a2d4a2204b54ba00e98a69bc474->leave($__internal_934be5a22a14ca44d760f3e8f5fced1bdc501a2d4a2204b54ba00e98a69bc474_prof);

    }

    public function getTemplateName()
    {
        return "workers/worker.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 26,  127 => 25,  121 => 23,  115 => 20,  112 => 19,  109 => 18,  105 => 17,  102 => 16,  97 => 15,  91 => 14,  83 => 11,  74 => 9,  70 => 8,  67 => 7,  61 => 6,  49 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'table_responsive.html.twig' %}*/
/* */
/* {% block title %}Workers{% endblock %}*/
/* {% block pageheader %}Workers{% endblock %}*/
/* */
/* {% block thead %}*/
/*     <tr>*/
/*         {% for name in columns_names %}*/
/*             <th>{{ name }}</th>*/
/*         {% endfor %}*/
/*     </tr>*/
/* {% endblock %}*/
/* */
/* {% block tbody %}*/
/*     {% for result in env_results %}*/
/*         <tr>*/
/*             {% for key,value in result|to_array %}*/
/*                 {% if key == 'environment' %}*/
/*                     <td>*/
/*                         <a href="{{ path('environment_show', {'environment_id': value.id}) }}"><i class="fa fa-pencil-square-o"></i></a>*/
/*                     </td>*/
/*                 {% else %}*/
/*                     <td>{{ value }}</td>*/
/*                 {% endif %}*/
/*             {% endfor %}*/
/*         </tr>*/
/*     {% endfor %}*/
/* {% endblock %}*/
