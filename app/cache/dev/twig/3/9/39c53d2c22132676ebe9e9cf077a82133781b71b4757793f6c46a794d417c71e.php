<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_ffaf34c2f4bfb972084aba860dbdb3ee819a7477a52d591ed2d9a0a0cff4482a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("TwigBundle::layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "TwigBundle::layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2520da79243da9df38bd20d29baf2d36537b2add00c0af408270200d35e0685d = $this->env->getExtension("native_profiler");
        $__internal_2520da79243da9df38bd20d29baf2d36537b2add00c0af408270200d35e0685d->enter($__internal_2520da79243da9df38bd20d29baf2d36537b2add00c0af408270200d35e0685d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_2520da79243da9df38bd20d29baf2d36537b2add00c0af408270200d35e0685d->leave($__internal_2520da79243da9df38bd20d29baf2d36537b2add00c0af408270200d35e0685d_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_265492b6af65dfd48143414c12f3a83f9eefd3530ef32cbac03831fbe419c823 = $this->env->getExtension("native_profiler");
        $__internal_265492b6af65dfd48143414c12f3a83f9eefd3530ef32cbac03831fbe419c823->enter($__internal_265492b6af65dfd48143414c12f3a83f9eefd3530ef32cbac03831fbe419c823_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('request')->generateAbsoluteUrl($this->env->getExtension('asset')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_265492b6af65dfd48143414c12f3a83f9eefd3530ef32cbac03831fbe419c823->leave($__internal_265492b6af65dfd48143414c12f3a83f9eefd3530ef32cbac03831fbe419c823_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_1c1757e005f0ce42722d33b4408d8869a356c94aa6f1cd8bbaf6be135d4e8167 = $this->env->getExtension("native_profiler");
        $__internal_1c1757e005f0ce42722d33b4408d8869a356c94aa6f1cd8bbaf6be135d4e8167->enter($__internal_1c1757e005f0ce42722d33b4408d8869a356c94aa6f1cd8bbaf6be135d4e8167_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_1c1757e005f0ce42722d33b4408d8869a356c94aa6f1cd8bbaf6be135d4e8167->leave($__internal_1c1757e005f0ce42722d33b4408d8869a356c94aa6f1cd8bbaf6be135d4e8167_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_8941abfa78afbd8ca9058b5c7d8c026bab8c7c5fc0a063630456beebdb9f6e48 = $this->env->getExtension("native_profiler");
        $__internal_8941abfa78afbd8ca9058b5c7d8c026bab8c7c5fc0a063630456beebdb9f6e48->enter($__internal_8941abfa78afbd8ca9058b5c7d8c026bab8c7c5fc0a063630456beebdb9f6e48_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("TwigBundle:Exception:exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_8941abfa78afbd8ca9058b5c7d8c026bab8c7c5fc0a063630456beebdb9f6e48->leave($__internal_8941abfa78afbd8ca9058b5c7d8c026bab8c7c5fc0a063630456beebdb9f6e48_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }
}
/* {% extends 'TwigBundle::layout.html.twig' %}*/
/* */
/* {% block head %}*/
/*     <link href="{{ absolute_url(asset('bundles/framework/css/exception.css')) }}" rel="stylesheet" type="text/css" media="all" />*/
/* {% endblock %}*/
/* */
/* {% block title %}*/
/*     {{ exception.message }} ({{ status_code }} {{ status_text }})*/
/* {% endblock %}*/
/* */
/* {% block body %}*/
/*     {% include 'TwigBundle:Exception:exception.html.twig' %}*/
/* {% endblock %}*/
/* */
