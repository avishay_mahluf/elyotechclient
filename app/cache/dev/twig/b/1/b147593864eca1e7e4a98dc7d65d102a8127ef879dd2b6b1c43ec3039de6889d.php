<?php

/* environment/environment.html.twig */
class __TwigTemplate_72127d1c03010ac3349a2f4cc795a4f117a433795c8ab1d0df534e6fd7575f1a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("table_responsive.html.twig", "environment/environment.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'pageheader' => array($this, 'block_pageheader'),
            'thead' => array($this, 'block_thead'),
            'tbody' => array($this, 'block_tbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "table_responsive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b4e90a533eeedbc3d287992e6eef7bdfbfec17467b8412e3a81ba3cda5facb2d = $this->env->getExtension("native_profiler");
        $__internal_b4e90a533eeedbc3d287992e6eef7bdfbfec17467b8412e3a81ba3cda5facb2d->enter($__internal_b4e90a533eeedbc3d287992e6eef7bdfbfec17467b8412e3a81ba3cda5facb2d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "environment/environment.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b4e90a533eeedbc3d287992e6eef7bdfbfec17467b8412e3a81ba3cda5facb2d->leave($__internal_b4e90a533eeedbc3d287992e6eef7bdfbfec17467b8412e3a81ba3cda5facb2d_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_2321b74ffae4ab9fc03e2e15dd9127c605e6e0ff252afcb2d56b0ef94bbf7022 = $this->env->getExtension("native_profiler");
        $__internal_2321b74ffae4ab9fc03e2e15dd9127c605e6e0ff252afcb2d56b0ef94bbf7022->enter($__internal_2321b74ffae4ab9fc03e2e15dd9127c605e6e0ff252afcb2d56b0ef94bbf7022_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Environment";
        
        $__internal_2321b74ffae4ab9fc03e2e15dd9127c605e6e0ff252afcb2d56b0ef94bbf7022->leave($__internal_2321b74ffae4ab9fc03e2e15dd9127c605e6e0ff252afcb2d56b0ef94bbf7022_prof);

    }

    // line 4
    public function block_pageheader($context, array $blocks = array())
    {
        $__internal_621c9b10ec090b7c9f4949239ac0f3961e2ab029e5714e1c22e28e5b1d2c2ae9 = $this->env->getExtension("native_profiler");
        $__internal_621c9b10ec090b7c9f4949239ac0f3961e2ab029e5714e1c22e28e5b1d2c2ae9->enter($__internal_621c9b10ec090b7c9f4949239ac0f3961e2ab029e5714e1c22e28e5b1d2c2ae9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pageheader"));

        echo "Environment";
        
        $__internal_621c9b10ec090b7c9f4949239ac0f3961e2ab029e5714e1c22e28e5b1d2c2ae9->leave($__internal_621c9b10ec090b7c9f4949239ac0f3961e2ab029e5714e1c22e28e5b1d2c2ae9_prof);

    }

    // line 6
    public function block_thead($context, array $blocks = array())
    {
        $__internal_4734316f9f0b88b9dea22a68752bdc4f8dfb3344d0a7fe3310ffe043ff611b65 = $this->env->getExtension("native_profiler");
        $__internal_4734316f9f0b88b9dea22a68752bdc4f8dfb3344d0a7fe3310ffe043ff611b65->enter($__internal_4734316f9f0b88b9dea22a68752bdc4f8dfb3344d0a7fe3310ffe043ff611b65_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "thead"));

        // line 7
        echo "    <tr>
    <th>Edit</th>
    ";
        // line 9
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["columns_names"]) ? $context["columns_names"] : $this->getContext($context, "columns_names")));
        foreach ($context['_seq'] as $context["_key"] => $context["name"]) {
            // line 10
            echo "        <th>";
            echo twig_escape_filter($this->env, $context["name"], "html", null, true);
            echo "</th>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['name'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 12
        echo "    </tr>
";
        
        $__internal_4734316f9f0b88b9dea22a68752bdc4f8dfb3344d0a7fe3310ffe043ff611b65->leave($__internal_4734316f9f0b88b9dea22a68752bdc4f8dfb3344d0a7fe3310ffe043ff611b65_prof);

    }

    // line 15
    public function block_tbody($context, array $blocks = array())
    {
        $__internal_8561ec63f721f81bf883c7a907abe85f0d343bb8e9cdbd0c8c07747c9f823cff = $this->env->getExtension("native_profiler");
        $__internal_8561ec63f721f81bf883c7a907abe85f0d343bb8e9cdbd0c8c07747c9f823cff->enter($__internal_8561ec63f721f81bf883c7a907abe85f0d343bb8e9cdbd0c8c07747c9f823cff_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tbody"));

        // line 16
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["env_results"]) ? $context["env_results"] : $this->getContext($context, "env_results")));
        foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
            // line 17
            echo "        <tr>
            <td>
                <a href=\"";
            // line 19
            echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("environment_show", array("environment_id" => $this->getAttribute($context["result"], "id", array()))), "html", null, true);
            echo "\"><i class=\"fa fa-pencil-square-o\"></i></a>
            </td>
            ";
            // line 21
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->env->getExtension('to_array_extension')->to_array($context["result"]));
            foreach ($context['_seq'] as $context["_key"] => $context["value"]) {
                // line 22
                echo "                <td>";
                echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                echo "</td>
            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['_key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 24
            echo "        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_8561ec63f721f81bf883c7a907abe85f0d343bb8e9cdbd0c8c07747c9f823cff->leave($__internal_8561ec63f721f81bf883c7a907abe85f0d343bb8e9cdbd0c8c07747c9f823cff_prof);

    }

    public function getTemplateName()
    {
        return "environment/environment.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  125 => 24,  116 => 22,  112 => 21,  107 => 19,  103 => 17,  98 => 16,  92 => 15,  84 => 12,  75 => 10,  71 => 9,  67 => 7,  61 => 6,  49 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'table_responsive.html.twig' %}*/
/* */
/* {% block title %}Environment{% endblock %}*/
/* {% block pageheader %}Environment{% endblock %}*/
/* */
/* {% block thead %}*/
/*     <tr>*/
/*     <th>Edit</th>*/
/*     {% for name in columns_names %}*/
/*         <th>{{ name }}</th>*/
/*     {% endfor %}*/
/*     </tr>*/
/* {% endblock %}*/
/* */
/* {% block tbody %}*/
/*     {% for result in env_results %}*/
/*         <tr>*/
/*             <td>*/
/*                 <a href="{{ path('environment_show', {'environment_id': result.id}) }}"><i class="fa fa-pencil-square-o"></i></a>*/
/*             </td>*/
/*             {% for value in result|to_array %}*/
/*                 <td>{{ value }}</td>*/
/*             {% endfor %}*/
/*         </tr>*/
/*     {% endfor %}*/
/* {% endblock %}*/
