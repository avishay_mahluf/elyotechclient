<?php

/* table_responsive.html.twig */
class __TwigTemplate_4bdde975092f5a23eccf485acf6d6716de0cc6467eb4acacb4b031d91e1ba806 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("blank.html.twig", "table_responsive.html.twig", 1);
        $this->blocks = array(
            'page_content' => array($this, 'block_page_content'),
            'pageheader' => array($this, 'block_pageheader'),
            'thead' => array($this, 'block_thead'),
            'tbody' => array($this, 'block_tbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "blank.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ce1db33149fb9388a9b0bbed6e4174da309a30c0fb035f81accb8541653c9439 = $this->env->getExtension("native_profiler");
        $__internal_ce1db33149fb9388a9b0bbed6e4174da309a30c0fb035f81accb8541653c9439->enter($__internal_ce1db33149fb9388a9b0bbed6e4174da309a30c0fb035f81accb8541653c9439_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "table_responsive.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ce1db33149fb9388a9b0bbed6e4174da309a30c0fb035f81accb8541653c9439->leave($__internal_ce1db33149fb9388a9b0bbed6e4174da309a30c0fb035f81accb8541653c9439_prof);

    }

    // line 3
    public function block_page_content($context, array $blocks = array())
    {
        $__internal_f2ccc1d9571f04c9da954ec394cf908aa24886b0519b41e00be0b5cf692a1c9f = $this->env->getExtension("native_profiler");
        $__internal_f2ccc1d9571f04c9da954ec394cf908aa24886b0519b41e00be0b5cf692a1c9f->enter($__internal_f2ccc1d9571f04c9da954ec394cf908aa24886b0519b41e00be0b5cf692a1c9f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_content"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-sm-12\">

            <h1 class=\"page-header\">";
        // line 7
        $this->displayBlock('pageheader', $context, $blocks);
        echo "</h1>

            <div class=\"table-responsive\">
                <table class=\"table\">
                    <thead>
                    ";
        // line 12
        $this->displayBlock('thead', $context, $blocks);
        // line 13
        echo "                    </thead>
                    <tbody>
                    ";
        // line 15
        $this->displayBlock('tbody', $context, $blocks);
        // line 16
        echo "                    </tbody>
                </table>
                <!-- /.table -->
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
";
        
        $__internal_f2ccc1d9571f04c9da954ec394cf908aa24886b0519b41e00be0b5cf692a1c9f->leave($__internal_f2ccc1d9571f04c9da954ec394cf908aa24886b0519b41e00be0b5cf692a1c9f_prof);

    }

    // line 7
    public function block_pageheader($context, array $blocks = array())
    {
        $__internal_9af180cffadf54c2340c99d6b9723688dde0d44e9018dfca2f8ea59bc2ef5bb3 = $this->env->getExtension("native_profiler");
        $__internal_9af180cffadf54c2340c99d6b9723688dde0d44e9018dfca2f8ea59bc2ef5bb3->enter($__internal_9af180cffadf54c2340c99d6b9723688dde0d44e9018dfca2f8ea59bc2ef5bb3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pageheader"));

        
        $__internal_9af180cffadf54c2340c99d6b9723688dde0d44e9018dfca2f8ea59bc2ef5bb3->leave($__internal_9af180cffadf54c2340c99d6b9723688dde0d44e9018dfca2f8ea59bc2ef5bb3_prof);

    }

    // line 12
    public function block_thead($context, array $blocks = array())
    {
        $__internal_df189ec8bfc28a2e7bd0dc71637fa6a69d0d9ce525d284705522bb41b356c7cf = $this->env->getExtension("native_profiler");
        $__internal_df189ec8bfc28a2e7bd0dc71637fa6a69d0d9ce525d284705522bb41b356c7cf->enter($__internal_df189ec8bfc28a2e7bd0dc71637fa6a69d0d9ce525d284705522bb41b356c7cf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "thead"));

        
        $__internal_df189ec8bfc28a2e7bd0dc71637fa6a69d0d9ce525d284705522bb41b356c7cf->leave($__internal_df189ec8bfc28a2e7bd0dc71637fa6a69d0d9ce525d284705522bb41b356c7cf_prof);

    }

    // line 15
    public function block_tbody($context, array $blocks = array())
    {
        $__internal_8be8151bd07a36b7f66ce04fdf0dc36bb6e3dc9fd35a2a75566e87f4047cf9c1 = $this->env->getExtension("native_profiler");
        $__internal_8be8151bd07a36b7f66ce04fdf0dc36bb6e3dc9fd35a2a75566e87f4047cf9c1->enter($__internal_8be8151bd07a36b7f66ce04fdf0dc36bb6e3dc9fd35a2a75566e87f4047cf9c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tbody"));

        
        $__internal_8be8151bd07a36b7f66ce04fdf0dc36bb6e3dc9fd35a2a75566e87f4047cf9c1->leave($__internal_8be8151bd07a36b7f66ce04fdf0dc36bb6e3dc9fd35a2a75566e87f4047cf9c1_prof);

    }

    public function getTemplateName()
    {
        return "table_responsive.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 15,  91 => 12,  80 => 7,  64 => 16,  62 => 15,  58 => 13,  56 => 12,  48 => 7,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'blank.html.twig' %}*/
/* */
/* {% block page_content %}*/
/*     <div class="row">*/
/*         <div class="col-sm-12">*/
/* */
/*             <h1 class="page-header">{% block pageheader %}{% endblock %}</h1>*/
/* */
/*             <div class="table-responsive">*/
/*                 <table class="table">*/
/*                     <thead>*/
/*                     {% block thead %}{% endblock %}*/
/*                     </thead>*/
/*                     <tbody>*/
/*                     {% block tbody %}{% endblock %}*/
/*                     </tbody>*/
/*                 </table>*/
/*                 <!-- /.table -->*/
/*             </div>*/
/*             <!-- /.table-responsive -->*/
/*         </div>*/
/*         <!-- /.col-lg-12 -->*/
/*     </div>*/
/*     <!-- /.row -->*/
/* {% endblock %}*/
