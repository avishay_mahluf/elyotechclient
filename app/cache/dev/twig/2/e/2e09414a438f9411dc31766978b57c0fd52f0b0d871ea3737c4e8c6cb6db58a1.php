<?php

/* default/index.html.twig */
class __TwigTemplate_e614af22ffe5c273dae8ecba4546bbe68a0d29cf8958b37c9700649fd505dcb4 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("blank.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'page_content' => array($this, 'block_page_content'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "blank.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f48bf84dd3b38c05f6b4c63303949af202eeb6261736e0b2d019ea1630230e7e = $this->env->getExtension("native_profiler");
        $__internal_f48bf84dd3b38c05f6b4c63303949af202eeb6261736e0b2d019ea1630230e7e->enter($__internal_f48bf84dd3b38c05f6b4c63303949af202eeb6261736e0b2d019ea1630230e7e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_f48bf84dd3b38c05f6b4c63303949af202eeb6261736e0b2d019ea1630230e7e->leave($__internal_f48bf84dd3b38c05f6b4c63303949af202eeb6261736e0b2d019ea1630230e7e_prof);

    }

    // line 3
    public function block_page_content($context, array $blocks = array())
    {
        $__internal_6c6ee547a71eb0697bdf518a55c78038c9099ece9c9f9e107117077ef8ca8b83 = $this->env->getExtension("native_profiler");
        $__internal_6c6ee547a71eb0697bdf518a55c78038c9099ece9c9f9e107117077ef8ca8b83->enter($__internal_6c6ee547a71eb0697bdf518a55c78038c9099ece9c9f9e107117077ef8ca8b83_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_content"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-lg-12\">
            <h1 class=\"page-header\">Elyotech Test</h1>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
";
        
        $__internal_6c6ee547a71eb0697bdf518a55c78038c9099ece9c9f9e107117077ef8ca8b83->leave($__internal_6c6ee547a71eb0697bdf518a55c78038c9099ece9c9f9e107117077ef8ca8b83_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  40 => 4,  34 => 3,  11 => 1,);
    }
}
/* {% extends 'blank.html.twig' %}*/
/* */
/* {% block page_content %}*/
/*     <div class="row">*/
/*         <div class="col-lg-12">*/
/*             <h1 class="page-header">Elyotech Test</h1>*/
/*         </div>*/
/*         <!-- /.col-lg-12 -->*/
/*     </div>*/
/*     <!-- /.row -->*/
/* {% endblock %}*/
