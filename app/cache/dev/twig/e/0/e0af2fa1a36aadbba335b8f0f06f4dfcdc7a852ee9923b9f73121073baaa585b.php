<?php

/* navigation.html.twig */
class __TwigTemplate_900b92c9c63edb23773d781382a4a250daa0a0f6771e29537c40229fb56cad61 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a5dcc814e3d91d736a953c0ca65a704710dfe79de2a90c43d610507aa66a5a67 = $this->env->getExtension("native_profiler");
        $__internal_a5dcc814e3d91d736a953c0ca65a704710dfe79de2a90c43d610507aa66a5a67->enter($__internal_a5dcc814e3d91d736a953c0ca65a704710dfe79de2a90c43d610507aa66a5a67_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "navigation.html.twig"));

        // line 1
        echo "<!-- Navigation -->
<nav class=\"navbar navbar-default navbar-static-top\" role=\"navigation\" style=\"margin-bottom: 0\">
    <div class=\"navbar-header\">
        <button type=\"button\" class=\"navbar-toggle\" data-toggle=\"collapse\" data-target=\".navbar-collapse\">
            <span class=\"sr-only\">Toggle navigation</span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
            <span class=\"icon-bar\"></span>
        </button>
        <a class=\"navbar-brand\" href=\"";
        // line 10
        echo $this->env->getExtension('routing')->getPath("homepage");
        echo "\">Elyotech Test</a>
    </div>
    <!-- navbar-header -->

    <ul class=\"nav navbar-top-links navbar-right\">
        <!-- dropdown -->
        <li class=\"dropdown\">
            <a class=\"dropdown-toggle\" data-toggle=\"dropdown\" href=\"#\">
                <i class=\"fa fa-user fa-fw\"></i>  <i class=\"fa fa-caret-down\"></i>
            </a>
            <ul class=\"dropdown-menu dropdown-user\">
                <li><a href=\"";
        // line 21
        echo $this->env->getExtension('routing')->getPath("login");
        echo "\"><i class=\"fa fa-sign-out fa-fw\"></i> Logout</a>
                </li>
            </ul>
            <!-- dropdown-user -->
        </li>
        <!-- dropdown -->
    </ul>
    <!-- navbar-top-links -->

    <div class=\"navbar-default sidebar\" role=\"navigation\">
        <div class=\"sidebar-nav navbar-collapse\">
            <ul class=\"nav\" id=\"side-menu\">
                <li>
                    <a href=\"";
        // line 34
        echo $this->env->getExtension('routing')->getPath("environment");
        echo "\"><i class=\"fa fa-cogs fa-fw\"></i> Environment</a>
                </li>
                <li>
                    <a href=\"";
        // line 37
        echo $this->env->getExtension('routing')->getPath("worker");
        echo "\"><i class=\"fa fa-user fa-fw\"></i> Workers</a>
                </li>
                <li>
                    <a href=\"";
        // line 40
        echo $this->env->getExtension('routing')->getPath("audit_log");
        echo "\"><i class=\"fa fa-list fa-fw\"></i> Audit Log</a>
                </li>
            </ul>
        </div>
        <!-- sidebar-collapse -->
    </div>
    <!-- navbar-static-side -->
</nav>";
        
        $__internal_a5dcc814e3d91d736a953c0ca65a704710dfe79de2a90c43d610507aa66a5a67->leave($__internal_a5dcc814e3d91d736a953c0ca65a704710dfe79de2a90c43d610507aa66a5a67_prof);

    }

    public function getTemplateName()
    {
        return "navigation.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  75 => 40,  69 => 37,  63 => 34,  47 => 21,  33 => 10,  22 => 1,);
    }
}
/* <!-- Navigation -->*/
/* <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">*/
/*     <div class="navbar-header">*/
/*         <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">*/
/*             <span class="sr-only">Toggle navigation</span>*/
/*             <span class="icon-bar"></span>*/
/*             <span class="icon-bar"></span>*/
/*             <span class="icon-bar"></span>*/
/*         </button>*/
/*         <a class="navbar-brand" href="{{ path('homepage') }}">Elyotech Test</a>*/
/*     </div>*/
/*     <!-- navbar-header -->*/
/* */
/*     <ul class="nav navbar-top-links navbar-right">*/
/*         <!-- dropdown -->*/
/*         <li class="dropdown">*/
/*             <a class="dropdown-toggle" data-toggle="dropdown" href="#">*/
/*                 <i class="fa fa-user fa-fw"></i>  <i class="fa fa-caret-down"></i>*/
/*             </a>*/
/*             <ul class="dropdown-menu dropdown-user">*/
/*                 <li><a href="{{ path('login') }}"><i class="fa fa-sign-out fa-fw"></i> Logout</a>*/
/*                 </li>*/
/*             </ul>*/
/*             <!-- dropdown-user -->*/
/*         </li>*/
/*         <!-- dropdown -->*/
/*     </ul>*/
/*     <!-- navbar-top-links -->*/
/* */
/*     <div class="navbar-default sidebar" role="navigation">*/
/*         <div class="sidebar-nav navbar-collapse">*/
/*             <ul class="nav" id="side-menu">*/
/*                 <li>*/
/*                     <a href="{{ path('environment') }}"><i class="fa fa-cogs fa-fw"></i> Environment</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ path('worker') }}"><i class="fa fa-user fa-fw"></i> Workers</a>*/
/*                 </li>*/
/*                 <li>*/
/*                     <a href="{{ path('audit_log') }}"><i class="fa fa-list fa-fw"></i> Audit Log</a>*/
/*                 </li>*/
/*             </ul>*/
/*         </div>*/
/*         <!-- sidebar-collapse -->*/
/*     </div>*/
/*     <!-- navbar-static-side -->*/
/* </nav>*/
