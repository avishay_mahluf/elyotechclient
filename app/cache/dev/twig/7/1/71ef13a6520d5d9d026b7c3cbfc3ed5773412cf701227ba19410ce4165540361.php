<?php

/* table_responsive.html.twig */
class __TwigTemplate_7121510f20e4f8620bb6ccc8fc13e553fccf0f5a22281848ca51e3d7f2960d96 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("blank.html.twig", "table_responsive.html.twig", 1);
        $this->blocks = array(
            'page_content' => array($this, 'block_page_content'),
            'pageheader' => array($this, 'block_pageheader'),
            'thead' => array($this, 'block_thead'),
            'tbody' => array($this, 'block_tbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "blank.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_092331a1fd46ff059870045e8df618d3e1e6fa1b923f102768ca0fd6bbc85da1 = $this->env->getExtension("native_profiler");
        $__internal_092331a1fd46ff059870045e8df618d3e1e6fa1b923f102768ca0fd6bbc85da1->enter($__internal_092331a1fd46ff059870045e8df618d3e1e6fa1b923f102768ca0fd6bbc85da1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "table_responsive.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_092331a1fd46ff059870045e8df618d3e1e6fa1b923f102768ca0fd6bbc85da1->leave($__internal_092331a1fd46ff059870045e8df618d3e1e6fa1b923f102768ca0fd6bbc85da1_prof);

    }

    // line 3
    public function block_page_content($context, array $blocks = array())
    {
        $__internal_c26a4a2eaa1a58ed0389bbb289aa94e6903e11f8d9fe66f1048a2c1ee130368f = $this->env->getExtension("native_profiler");
        $__internal_c26a4a2eaa1a58ed0389bbb289aa94e6903e11f8d9fe66f1048a2c1ee130368f->enter($__internal_c26a4a2eaa1a58ed0389bbb289aa94e6903e11f8d9fe66f1048a2c1ee130368f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_content"));

        // line 4
        echo "    <div class=\"row\">
        <div class=\"col-sm-12\">

            <h1 class=\"page-header\">";
        // line 7
        $this->displayBlock('pageheader', $context, $blocks);
        echo "</h1>

            <div class=\"table-responsive\">
                <table class=\"table\">
                    <thead>
                    ";
        // line 12
        $this->displayBlock('thead', $context, $blocks);
        // line 13
        echo "                    </thead>
                    <tbody>
                    ";
        // line 15
        $this->displayBlock('tbody', $context, $blocks);
        // line 16
        echo "                    </tbody>
                </table>
                <!-- /.table -->
            </div>
            <!-- /.table-responsive -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
";
        
        $__internal_c26a4a2eaa1a58ed0389bbb289aa94e6903e11f8d9fe66f1048a2c1ee130368f->leave($__internal_c26a4a2eaa1a58ed0389bbb289aa94e6903e11f8d9fe66f1048a2c1ee130368f_prof);

    }

    // line 7
    public function block_pageheader($context, array $blocks = array())
    {
        $__internal_7e5b8fad4719b130db5c34fadc0036b32175b15d3575d412978761c45f0e1304 = $this->env->getExtension("native_profiler");
        $__internal_7e5b8fad4719b130db5c34fadc0036b32175b15d3575d412978761c45f0e1304->enter($__internal_7e5b8fad4719b130db5c34fadc0036b32175b15d3575d412978761c45f0e1304_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pageheader"));

        
        $__internal_7e5b8fad4719b130db5c34fadc0036b32175b15d3575d412978761c45f0e1304->leave($__internal_7e5b8fad4719b130db5c34fadc0036b32175b15d3575d412978761c45f0e1304_prof);

    }

    // line 12
    public function block_thead($context, array $blocks = array())
    {
        $__internal_d5e3890b2db99f8c691bce78a8d1d71186a0a428608c23bdfa6ca724c9d49769 = $this->env->getExtension("native_profiler");
        $__internal_d5e3890b2db99f8c691bce78a8d1d71186a0a428608c23bdfa6ca724c9d49769->enter($__internal_d5e3890b2db99f8c691bce78a8d1d71186a0a428608c23bdfa6ca724c9d49769_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "thead"));

        
        $__internal_d5e3890b2db99f8c691bce78a8d1d71186a0a428608c23bdfa6ca724c9d49769->leave($__internal_d5e3890b2db99f8c691bce78a8d1d71186a0a428608c23bdfa6ca724c9d49769_prof);

    }

    // line 15
    public function block_tbody($context, array $blocks = array())
    {
        $__internal_33cb484e69c8df9785aca7e2487f30c66ce73322ab65690ed67647239af838de = $this->env->getExtension("native_profiler");
        $__internal_33cb484e69c8df9785aca7e2487f30c66ce73322ab65690ed67647239af838de->enter($__internal_33cb484e69c8df9785aca7e2487f30c66ce73322ab65690ed67647239af838de_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tbody"));

        
        $__internal_33cb484e69c8df9785aca7e2487f30c66ce73322ab65690ed67647239af838de->leave($__internal_33cb484e69c8df9785aca7e2487f30c66ce73322ab65690ed67647239af838de_prof);

    }

    public function getTemplateName()
    {
        return "table_responsive.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  102 => 15,  91 => 12,  80 => 7,  64 => 16,  62 => 15,  58 => 13,  56 => 12,  48 => 7,  43 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'blank.html.twig' %}*/
/* */
/* {% block page_content %}*/
/*     <div class="row">*/
/*         <div class="col-sm-12">*/
/* */
/*             <h1 class="page-header">{% block pageheader %}{% endblock %}</h1>*/
/* */
/*             <div class="table-responsive">*/
/*                 <table class="table">*/
/*                     <thead>*/
/*                     {% block thead %}{% endblock %}*/
/*                     </thead>*/
/*                     <tbody>*/
/*                     {% block tbody %}{% endblock %}*/
/*                     </tbody>*/
/*                 </table>*/
/*                 <!-- /.table -->*/
/*             </div>*/
/*             <!-- /.table-responsive -->*/
/*         </div>*/
/*         <!-- /.col-lg-12 -->*/
/*     </div>*/
/*     <!-- /.row -->*/
/* {% endblock %}*/
