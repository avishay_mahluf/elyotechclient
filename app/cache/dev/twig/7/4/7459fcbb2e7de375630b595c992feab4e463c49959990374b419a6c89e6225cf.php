<?php

/* blank.html.twig */
class __TwigTemplate_7cb813c30a32e1dfe84cfdb4a0529ef1481b86fa18a45d64df7c8d3feb8b06e8 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "blank.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'nav' => array($this, 'block_nav'),
            'page_content' => array($this, 'block_page_content'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5b622bf80d287645ce249e7713a881ed6bb9e40289996f9bacfdba7f566bfb87 = $this->env->getExtension("native_profiler");
        $__internal_5b622bf80d287645ce249e7713a881ed6bb9e40289996f9bacfdba7f566bfb87->enter($__internal_5b622bf80d287645ce249e7713a881ed6bb9e40289996f9bacfdba7f566bfb87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "blank.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_5b622bf80d287645ce249e7713a881ed6bb9e40289996f9bacfdba7f566bfb87->leave($__internal_5b622bf80d287645ce249e7713a881ed6bb9e40289996f9bacfdba7f566bfb87_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_3590ede97a0aabf23a8abcb03a796f1dda1631b46bb6505c95b0ffa32b8fe89c = $this->env->getExtension("native_profiler");
        $__internal_3590ede97a0aabf23a8abcb03a796f1dda1631b46bb6505c95b0ffa32b8fe89c->enter($__internal_3590ede97a0aabf23a8abcb03a796f1dda1631b46bb6505c95b0ffa32b8fe89c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div id=\"wrapper\">

        ";
        // line 6
        $this->displayBlock('nav', $context, $blocks);
        // line 9
        echo "
        <!-- Page Content -->
        <div id=\"page-wrapper\">
            <div class=\"container-fluid\">
                ";
        // line 13
        $this->displayBlock('page_content', $context, $blocks);
        // line 14
        echo "            </div>
            <!-- /.container-fluid -->
        </div>
        <!-- /#page-wrapper -->

    </div>
    ";
        
        $__internal_3590ede97a0aabf23a8abcb03a796f1dda1631b46bb6505c95b0ffa32b8fe89c->leave($__internal_3590ede97a0aabf23a8abcb03a796f1dda1631b46bb6505c95b0ffa32b8fe89c_prof);

    }

    // line 6
    public function block_nav($context, array $blocks = array())
    {
        $__internal_fe2e76a92bd2a226b41d60119ff85232a58654ea2ee26f0ad2dd59f7038ee103 = $this->env->getExtension("native_profiler");
        $__internal_fe2e76a92bd2a226b41d60119ff85232a58654ea2ee26f0ad2dd59f7038ee103->enter($__internal_fe2e76a92bd2a226b41d60119ff85232a58654ea2ee26f0ad2dd59f7038ee103_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "nav"));

        // line 7
        echo "            ";
        echo twig_include($this->env, $context, "navigation.html.twig");
        echo "
        ";
        
        $__internal_fe2e76a92bd2a226b41d60119ff85232a58654ea2ee26f0ad2dd59f7038ee103->leave($__internal_fe2e76a92bd2a226b41d60119ff85232a58654ea2ee26f0ad2dd59f7038ee103_prof);

    }

    // line 13
    public function block_page_content($context, array $blocks = array())
    {
        $__internal_f5274b5b3a309b00476ab5ac9168fcbe2dbb8794d0bd5d3c89e7743d0da567f1 = $this->env->getExtension("native_profiler");
        $__internal_f5274b5b3a309b00476ab5ac9168fcbe2dbb8794d0bd5d3c89e7743d0da567f1->enter($__internal_f5274b5b3a309b00476ab5ac9168fcbe2dbb8794d0bd5d3c89e7743d0da567f1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "page_content"));

        
        $__internal_f5274b5b3a309b00476ab5ac9168fcbe2dbb8794d0bd5d3c89e7743d0da567f1->leave($__internal_f5274b5b3a309b00476ab5ac9168fcbe2dbb8794d0bd5d3c89e7743d0da567f1_prof);

    }

    // line 23
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_d8820ad3bb652d44c0ac576131c7fb03c41c865d637f1373da2e7e06945670be = $this->env->getExtension("native_profiler");
        $__internal_d8820ad3bb652d44c0ac576131c7fb03c41c865d637f1373da2e7e06945670be->enter($__internal_d8820ad3bb652d44c0ac576131c7fb03c41c865d637f1373da2e7e06945670be_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 24
        echo "    ";
        echo twig_include($this->env, $context, "stylesheets.html.twig");
        echo "
";
        
        $__internal_d8820ad3bb652d44c0ac576131c7fb03c41c865d637f1373da2e7e06945670be->leave($__internal_d8820ad3bb652d44c0ac576131c7fb03c41c865d637f1373da2e7e06945670be_prof);

    }

    // line 28
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_914953939caa6cfc2eb2cc2540c69225f91ffc25f049446e9498224017abc381 = $this->env->getExtension("native_profiler");
        $__internal_914953939caa6cfc2eb2cc2540c69225f91ffc25f049446e9498224017abc381->enter($__internal_914953939caa6cfc2eb2cc2540c69225f91ffc25f049446e9498224017abc381_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 29
        echo "    ";
        echo twig_include($this->env, $context, "javascripts.html.twig");
        echo "
";
        
        $__internal_914953939caa6cfc2eb2cc2540c69225f91ffc25f049446e9498224017abc381->leave($__internal_914953939caa6cfc2eb2cc2540c69225f91ffc25f049446e9498224017abc381_prof);

    }

    public function getTemplateName()
    {
        return "blank.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  120 => 29,  114 => 28,  104 => 24,  98 => 23,  87 => 13,  77 => 7,  71 => 6,  58 => 14,  56 => 13,  50 => 9,  48 => 6,  44 => 4,  38 => 3,  11 => 1,);
    }
}
/* {% extends 'base.html.twig' %}*/
/* */
/* {% block body %}*/
/*     <div id="wrapper">*/
/* */
/*         {% block nav %}*/
/*             {{ include('navigation.html.twig') }}*/
/*         {% endblock %}*/
/* */
/*         <!-- Page Content -->*/
/*         <div id="page-wrapper">*/
/*             <div class="container-fluid">*/
/*                 {% block page_content %}{% endblock %}*/
/*             </div>*/
/*             <!-- /.container-fluid -->*/
/*         </div>*/
/*         <!-- /#page-wrapper -->*/
/* */
/*     </div>*/
/*     {# wrapper #}*/
/* {% endblock %}*/
/* */
/* {% block stylesheets %}*/
/*     {{ include('stylesheets.html.twig') }}*/
/* {% endblock %}*/
/* */
/* */
/* {% block javascripts %}*/
/*     {{ include('javascripts.html.twig') }}*/
/* {% endblock %}*/
