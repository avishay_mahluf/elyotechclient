<?php

/* workers/worker.html.twig */
class __TwigTemplate_bcb4e2ec06030309f8b73dc8c553c430da8845082fa386d363babf28a39ef16c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("table_responsive.html.twig", "workers/worker.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'pageheader' => array($this, 'block_pageheader'),
            'thead' => array($this, 'block_thead'),
            'tbody' => array($this, 'block_tbody'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "table_responsive.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7a58c4a3c6711cab98649e12e6ef12ac7354abdb8de28816f1904ef28e81dc87 = $this->env->getExtension("native_profiler");
        $__internal_7a58c4a3c6711cab98649e12e6ef12ac7354abdb8de28816f1904ef28e81dc87->enter($__internal_7a58c4a3c6711cab98649e12e6ef12ac7354abdb8de28816f1904ef28e81dc87_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "workers/worker.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7a58c4a3c6711cab98649e12e6ef12ac7354abdb8de28816f1904ef28e81dc87->leave($__internal_7a58c4a3c6711cab98649e12e6ef12ac7354abdb8de28816f1904ef28e81dc87_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_b28e006536842ea15105d75e6ec3c678f7346da5035e97d9f58b175d47dbcc08 = $this->env->getExtension("native_profiler");
        $__internal_b28e006536842ea15105d75e6ec3c678f7346da5035e97d9f58b175d47dbcc08->enter($__internal_b28e006536842ea15105d75e6ec3c678f7346da5035e97d9f58b175d47dbcc08_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Workers";
        
        $__internal_b28e006536842ea15105d75e6ec3c678f7346da5035e97d9f58b175d47dbcc08->leave($__internal_b28e006536842ea15105d75e6ec3c678f7346da5035e97d9f58b175d47dbcc08_prof);

    }

    // line 4
    public function block_pageheader($context, array $blocks = array())
    {
        $__internal_234c3d02be9c8c72cdc6c6ee67d707125841c5680fb8f1122a0dbea50c57be45 = $this->env->getExtension("native_profiler");
        $__internal_234c3d02be9c8c72cdc6c6ee67d707125841c5680fb8f1122a0dbea50c57be45->enter($__internal_234c3d02be9c8c72cdc6c6ee67d707125841c5680fb8f1122a0dbea50c57be45_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "pageheader"));

        echo "Workers";
        
        $__internal_234c3d02be9c8c72cdc6c6ee67d707125841c5680fb8f1122a0dbea50c57be45->leave($__internal_234c3d02be9c8c72cdc6c6ee67d707125841c5680fb8f1122a0dbea50c57be45_prof);

    }

    // line 6
    public function block_thead($context, array $blocks = array())
    {
        $__internal_b45cf462f6c100a106f85ce2753fbb865a84c6fe235b54058cd1708955075e6f = $this->env->getExtension("native_profiler");
        $__internal_b45cf462f6c100a106f85ce2753fbb865a84c6fe235b54058cd1708955075e6f->enter($__internal_b45cf462f6c100a106f85ce2753fbb865a84c6fe235b54058cd1708955075e6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "thead"));

        // line 7
        echo "    <tr>
        ";
        // line 8
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["columns_names"]) ? $context["columns_names"] : $this->getContext($context, "columns_names")));
        foreach ($context['_seq'] as $context["_key"] => $context["name"]) {
            // line 9
            echo "            <th>";
            echo twig_escape_filter($this->env, $context["name"], "html", null, true);
            echo "</th>
        ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['name'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 11
        echo "    </tr>
";
        
        $__internal_b45cf462f6c100a106f85ce2753fbb865a84c6fe235b54058cd1708955075e6f->leave($__internal_b45cf462f6c100a106f85ce2753fbb865a84c6fe235b54058cd1708955075e6f_prof);

    }

    // line 14
    public function block_tbody($context, array $blocks = array())
    {
        $__internal_6161079fcec8fec2d97e55ae39ff143bfa9129872cd88aeac52fed8dcfcf6094 = $this->env->getExtension("native_profiler");
        $__internal_6161079fcec8fec2d97e55ae39ff143bfa9129872cd88aeac52fed8dcfcf6094->enter($__internal_6161079fcec8fec2d97e55ae39ff143bfa9129872cd88aeac52fed8dcfcf6094_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "tbody"));

        // line 15
        echo "    ";
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["env_results"]) ? $context["env_results"] : $this->getContext($context, "env_results")));
        foreach ($context['_seq'] as $context["_key"] => $context["result"]) {
            // line 16
            echo "        <tr>
            ";
            // line 17
            $context['_parent'] = $context;
            $context['_seq'] = twig_ensure_traversable($this->env->getExtension('to_array_extension')->to_array($context["result"]));
            foreach ($context['_seq'] as $context["key"] => $context["value"]) {
                // line 18
                echo "                ";
                if (($context["key"] == "environment")) {
                    // line 19
                    echo "                    <td>
                        <a href=\"";
                    // line 20
                    echo twig_escape_filter($this->env, $this->env->getExtension('routing')->getPath("environment_show", array("environment_id" => $this->getAttribute($context["value"], "id", array()))), "html", null, true);
                    echo "\"><i class=\"fa fa-pencil-square-o\"></i></a>
                    </td>
                ";
                } else {
                    // line 23
                    echo "                    <td>";
                    echo twig_escape_filter($this->env, $context["value"], "html", null, true);
                    echo "</td>
                ";
                }
                // line 25
                echo "            ";
            }
            $_parent = $context['_parent'];
            unset($context['_seq'], $context['_iterated'], $context['key'], $context['value'], $context['_parent'], $context['loop']);
            $context = array_intersect_key($context, $_parent) + $_parent;
            // line 26
            echo "        </tr>
    ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['result'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        
        $__internal_6161079fcec8fec2d97e55ae39ff143bfa9129872cd88aeac52fed8dcfcf6094->leave($__internal_6161079fcec8fec2d97e55ae39ff143bfa9129872cd88aeac52fed8dcfcf6094_prof);

    }

    public function getTemplateName()
    {
        return "workers/worker.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  133 => 26,  127 => 25,  121 => 23,  115 => 20,  112 => 19,  109 => 18,  105 => 17,  102 => 16,  97 => 15,  91 => 14,  83 => 11,  74 => 9,  70 => 8,  67 => 7,  61 => 6,  49 => 4,  37 => 3,  11 => 1,);
    }
}
/* {% extends 'table_responsive.html.twig' %}*/
/* */
/* {% block title %}Workers{% endblock %}*/
/* {% block pageheader %}Workers{% endblock %}*/
/* */
/* {% block thead %}*/
/*     <tr>*/
/*         {% for name in columns_names %}*/
/*             <th>{{ name }}</th>*/
/*         {% endfor %}*/
/*     </tr>*/
/* {% endblock %}*/
/* */
/* {% block tbody %}*/
/*     {% for result in env_results %}*/
/*         <tr>*/
/*             {% for key,value in result|to_array %}*/
/*                 {% if key == 'environment' %}*/
/*                     <td>*/
/*                         <a href="{{ path('environment_show', {'environment_id': value.id}) }}"><i class="fa fa-pencil-square-o"></i></a>*/
/*                     </td>*/
/*                 {% else %}*/
/*                     <td>{{ value }}</td>*/
/*                 {% endif %}*/
/*             {% endfor %}*/
/*         </tr>*/
/*     {% endfor %}*/
/* {% endblock %}*/
